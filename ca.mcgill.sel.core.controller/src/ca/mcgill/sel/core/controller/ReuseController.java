package ca.mcgill.sel.core.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * The controller for {link @COREReuse and @COREModelReuse}.
 *
 * @author oalam
 */
public class ReuseController extends CoreBaseController {
    
    /**
     * Creates a new instance of {@link ReuseController}.
     */
    protected ReuseController() {
        // prevent anyone outside this package to instantiate
    }

    /**
     * Creates a new {@link COREReuse} and {@link COREModelReuse} for the given models.
     *
     * @param owner the model that is reusing
     * @param externalArtefact the referenced model (external woven model for models of selected features)
     * @param configuration the configuration for the reuse
     * @param reuse the reuse that is configured
     */
    public void createModelReuse(COREArtefact owner, COREArtefact externalArtefact, 
            COREConfiguration configuration, COREReuse reuse) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        
        CompoundCommand addModelReuseCommand = new CompoundCommand();
        
        // create the model reuse with: a reuse, a configuration and an external artefact
        COREModelReuse modelReuse = CoreFactory.eINSTANCE.createCOREModelReuse();
        modelReuse.setSource(externalArtefact);
        modelReuse.setConfiguration(configuration);
        
        Command setReuseCommand = SetCommand.create(editingDomain, modelReuse,
                CorePackage.Literals.CORE_MODEL_REUSE__REUSE, reuse);
        addModelReuseCommand.append(setReuseCommand);
        
        Command addModelCommand = AddCommand.create(editingDomain, owner,
                CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
        addModelReuseCommand.append(addModelCommand);
        
        doExecute(editingDomain, addModelReuseCommand);     
    }
    
    /**
     * Creates a new {@link COREReuse} and {@link COREModelReuse} for the given models.
     *
     * @param owner the model that is reusing
     * @param reusingConcern the concern that the model belong to
     * @param reusedConcern the reused concern
     * @param externalArtefact the referenced model (external woven model for models of selected features)
     * @param configuration the configuration for the reuse
     */
    public void createNewReuseAndModelReuse(COREArtefact owner, COREConcern reusingConcern, COREConcern reusedConcern,
            COREArtefact externalArtefact, COREConfiguration configuration) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // create new reuse
        COREReuse reuse = COREModelUtil.createReuse(reusingConcern, reusedConcern);
               
        CompoundCommand compoundCommandConcern = new CompoundCommand();

        // Add the reuse to the concern
        Command addCOREReuseCommand = AddCommand.create(editingDomain, reusingConcern,
                CorePackage.Literals.CORE_CONCERN__REUSES, reuse);
        compoundCommandConcern.append(addCOREReuseCommand);

        // initialize the configuration's properties
        configuration.setSource(reusedConcern.getFeatureModel());
        //configuration.setExtendedReuse(reuse);
        
        // create a name for the configuration
        String configurationName = reusedConcern.getName() + "_";
        for (COREFeature feature : configuration.getSelected()) {
            configurationName += feature.getName();
        }
        configuration.setName(configurationName);

        COREModelReuse modelReuse = CoreFactory.eINSTANCE.createCOREModelReuse();
        modelReuse.setSource(externalArtefact);
        modelReuse.setConfiguration(configuration);
        modelReuse.setReuse(reuse);
                
        Command addCOREModelReuseCommand = AddCommand.create(editingDomain, owner,
                CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
        compoundCommandConcern.append(addCOREModelReuseCommand);
        
        doExecute(editingDomain, compoundCommandConcern);
    }
    
    /**
     * Creates a new {@link COREModelExtension} for the given model.
     *
     * @param owner the model that is reusing
     * @param extendedModel the referenced model
     */
    public void createModelExtension(COREArtefact owner, COREArtefact extendedModel) {
        COREModelExtension extension = COREModelUtil.createModelExtension(extendedModel);
        doAdd(owner, CorePackage.Literals.CORE_ARTEFACT__MODEL_EXTENSIONS, extension);
    }
    
    /**
     * Removes a model Reuse.
     *
     * @param modelReuse to be removed
     */
    public void removeModelReuse(COREModelReuse modelReuse) {
        
        COREReuse reuse = modelReuse.getReuse();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(reuse);
        
        CompoundCommand compoundCommand = new CompoundCommand();
        
        Command artefactCommand = RemoveCommand.create(editingDomain, modelReuse.eContainer(), 
                CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse);
        compoundCommand.append(artefactCommand);
        
        Command command = RemoveCommand
                .create(editingDomain, reuse, CorePackage.Literals.CORE_REUSE__MODEL_REUSES, modelReuse);
        compoundCommand.append(command);
        
        if (reuse.getModelReuses().size() == 1) {
            Command reuseCommand = RemoveCommand.create(editingDomain, reuse.eContainer(),
                    CorePackage.Literals.CORE_CONCERN__REUSES, reuse);
            compoundCommand.append(reuseCommand);
        }

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes a model Extension.
     *
     * @param modelExtension to be removed
     */
    public void removeModelExtension(COREModelExtension modelExtension) {
        doRemove(modelExtension);
    }

    /**
     * Delete the given COREReuse.
     *
     * @param reuse the COREReuse to be deleted
     */
    public void deleteCOREReuse(COREReuse reuse) {
        doRemove(reuse);
    }

}
