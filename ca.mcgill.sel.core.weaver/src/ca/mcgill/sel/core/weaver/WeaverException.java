package ca.mcgill.sel.core.weaver;

/**
 * Thrown when there are problems with a weaver.
 * 
 * @author Joerg
 *
 */
public class WeaverException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * @param comment any comment that should be displayed in case the exception is printed.
     */
    public WeaverException(String comment) {
        super(comment);
    }
    
    /**
     * Constructor.
     * @param exceptionToWrap the exception that should be wrapped inside a WeavingException.
     */
    public WeaverException(Exception exceptionToWrap) {
        super(exceptionToWrap);
    }
}
