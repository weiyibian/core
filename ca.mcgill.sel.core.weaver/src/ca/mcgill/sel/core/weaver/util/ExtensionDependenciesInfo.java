package ca.mcgill.sel.core.weaver.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * Stores information about the current extension dependencies for a given configuration of a concern.
 * This class has methods that allows the COREWeaver to weave a set of COREArtefacts
 * starting with the external artefacts that has the lowest depth rank in the
 * dependency tree.
 * 
 * @author joerg
 */
public class ExtensionDependenciesInfo {

    // Used to weave extends
    private int maxDepth;
    private HashMap<COREExternalArtefact, Integer> ranks;

    // Keep track of the external artefacts that need to be woven for this concern.
    private List<COREExternalArtefact> artefactsToWeave;

    /**
     * The constructor.
     */
    public ExtensionDependenciesInfo() {
        this.ranks = new HashMap<>();
        this.artefactsToWeave = new ArrayList<COREExternalArtefact>();
    }

    /**
     * Constructor for {@link ExtensionDependenciesInfo}.
     *
     * @param configuration - configuration containing information about selected features.
     */
    public ExtensionDependenciesInfo(COREConfiguration configuration) {
        this();
        if (configuration != null) {
            // Get scenes to weave
            Set<COREFeature> selectedFeatures = new HashSet<COREFeature>(configuration.getSelected());
            Set<COREScene> resolvedScenes =
                    COREModelUtil.getRealizationScenes(selectedFeatures);

            if (resolvedScenes != null) {
                for (COREScene scene : resolvedScenes) {
                    //TODO: this is currently hardcoded assuming there is only one artefact per scene, i.e.
                    // only one language per perspective
                    this.artefactsToWeave.add((COREExternalArtefact) scene.getArtefacts().values()
                            .iterator().next().get(0));                    
                }
            }
            
            // Determine the extension dependency depth of each artefact
            for (COREExternalArtefact artefact : artefactsToWeave) {
                determineAndSetRanks(artefact);
            }
            // Determine the maximum depth
            maxDepth = 0;
            for (int i : ranks.values()) {
                if (i > maxDepth) {
                    maxDepth = i;
                }
            }
        }
    }

    /**
     * Helper method that recursively determines the extension hierarchy rank for a given artefact and
     * all artefacts it extends.
     * 
     * @param artefact the bottom of the extension hierarchy that is to be considered
     * @return the rank of the artefact
     */
    private int determineAndSetRanks(COREExternalArtefact artefact) {
        Integer i = ranks.get(artefact);
        if (i != null) {
            return i.intValue();  
        } else if (artefact.getModelExtensions().size() == 0) {
            ranks.put(artefact, 0);
            return 0;
        } else {
            int maxRank = 0;
            for (COREModelExtension ext : artefact.getModelExtensions()) {
                int extendedRank = determineAndSetRanks((COREExternalArtefact) ext.getSource());
                if (extendedRank >= maxRank) {
                    maxRank = extendedRank + 1;
                }
            }
            ranks.put(artefact, maxRank);
            return maxRank;
        }
    }
    
    /**
     * Returns the set of COREExternalArtefacts of a given rank in the extension hierarchy.
     * 
     * @param desiredRank the rank 
     * @return the found artefacts
     */
    public Set<COREExternalArtefact> getArtefactsOfRank(int desiredRank) {
        HashSet<COREExternalArtefact> result = new HashSet<COREExternalArtefact>();
        
        for (COREExternalArtefact artefact : ranks.keySet()) {
            if (ranks.get(artefact) == desiredRank) {
                result.add(artefact);
            }
        }
        return result;
    }
    
    /**
     * Get the list of artefacts woven for this given concern.
     *
     * @return the woven artefacts.
     */
    public List<COREExternalArtefact> getWovenArtefacts() {
        return artefactsToWeave;
    }    
    
    /**
     * Returns the maximum depth of the dependency hierarchy.
     * 
     * @return the maximum depth
     */
    public int getMaxDepth() {
        return maxDepth;
    }
}

