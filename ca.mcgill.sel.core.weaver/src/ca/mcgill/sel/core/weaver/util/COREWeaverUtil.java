package ca.mcgill.sel.core.weaver.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.weaver.COREWeaver;

/**
 * Helper class with convenient static methods for working with the {@link COREWeaver}.
 *
 * @author cbensoussan
 * @author joerg
 */
public final class COREWeaverUtil {

    /**
     * The file extension for aspects.
     */
    public static final String ASPECT_FILE_EXTENSION = "ram";

    /**
     * String appended to woven aspect when saved.
     */
    public static final String WOVEN_PREFIX = "Woven_";

    /**
     * Maximum file name length to avoid problems on operating systems with limitations.
     */
    private static final int MAXIMUM_FILENAME_LENGTH = 240;

    /**
     * Creates a new instance of {@link COREWeaverUtil}.
     */
    private COREWeaverUtil() {
        // suppress default constructor
    }

    /**
     * Combines an artefact name based on the concern and the selected features.
     * The format is the following: "concernname"<"concatenated list of selected features">,
     * e.g., Authentication&lt;PasswordExpiry,Password&gt;
     * If the resulting filename would be longer than 240 characters, then the comma separated
     * list of features is replaced by a hash of the entire string. This is done because
     * of operating systems (e.g. Windows) that still have a limit on file name lengths. 
     * 
     * @param concern the reused concern
     * @param selectedArtefacts the selected artefacts of the concern that will be woven
     * @return the name
     */
    public static String createDefaultModelName(COREConcern concern,
            Collection<COREExternalArtefact> selectedArtefacts) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(concern.getName());
        stringBuilder.append("<");
        
        String intermediateResult = stringBuilder.toString();

        for (COREArtefact artefact : selectedArtefacts) {
            stringBuilder.append(artefact.getName());
            stringBuilder.append(",");
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append(">");

        String result = stringBuilder.toString();
        if (result.length() > MAXIMUM_FILENAME_LENGTH) {
            result = intermediateResult + result.hashCode() + ">";
        }
        return result;
    }

    /**
     * Create a String name for the given concern's woven aspect.
     * Will create a name of type:
     * Woven_{ConcernName}_{name of Woven Aspects from the concern}+_([ReuseName]_{Woven Aspects from the reuses})*
     *
     * @param concern - The concern that is being woven.
     * @param cwi - The {@link DependencyWeavingInfo} containing hierarchy of reuses and resolved aspects.
     * @return the string for this particular selection.
     */
    public static String createWovenConcernFilename(COREConcern concern, ExtensionDependenciesInfo cwi) {
        
        StringBuilder stringBuilder = new StringBuilder();

        // Concern name
        stringBuilder.append(WOVEN_PREFIX.concat(concern.getName()));

        // Woven aspects
        stringBuilder.append(getResolvedModelsToString(cwi));

        // Save the woven aspect in the Reused_ConcernName folder.
        URI fileURI = concern.eResource().getURI();
        fileURI = fileURI.trimSegments(1);
        fileURI = fileURI.appendSegment(stringBuilder.toString());
        fileURI = fileURI.appendFileExtension(ASPECT_FILE_EXTENSION);

        return fileURI.toFileString();
    }

    /**
     * Create an alphabetically-ordered string of form:
     * AspectA_AspectB_AspectC(_ReuseA_AspectA2_AspectB2_..)* based on the given {@link DependencyWeavingInfo}.
     * The name of each aspect is the name of its serialized file.
     *
     * @param cwi - The {@link DependencyWeavingInfo} containing hierarchy of reuses and resolved aspects.
     * @return the string for this particular selection.
     */
    private static String getResolvedModelsToString(ExtensionDependenciesInfo cwi) {
        List<COREExternalArtefact> resolvedModel = cwi.getWovenArtefacts();

        StringBuilder stringBuilder = new StringBuilder();
        String[] selectedModelsName = new String[resolvedModel.size()];

        int i = 0;
        for (COREExternalArtefact model : resolvedModel) {
            // Use file name instead of model name to avoid problems with invalid characters.
            EObject rootOfModel = model.getRootModelElement();
            selectedModelsName[i++] = rootOfModel.eResource()
                    .getURI().trimFileExtension().lastSegment();
        }
        Arrays.sort(selectedModelsName);

        for (i = 0; i < selectedModelsName.length; i++) {
            stringBuilder.append("_");
            stringBuilder.append(selectedModelsName[i]);
        }
        
//TODO: Take care of child reuses
//        for (COREReuse reuse : cwi.getChildrenWeavingInfo().keySet()) {
//            stringBuilder.append("_" + reuse.getName());
//            stringBuilder.append(getResolvedModelsToString(cwi.getChildrenWeavingInfo().get(reuse)));
//        }

        return stringBuilder.toString();
    }
}
