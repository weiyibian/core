package ca.mcgill.sel.core.language.registry;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.resource.Resource.Factory;

import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.weaver.COREModelWeaver;
import ca.mcgill.sel.core.weaver.COREWeaver;


/**
 * CORELanguageRegistry is a helper class to register and 
 * keep track of languages supported by CORE.
 * 
 * @author Hyacinth
 * @author ddevine
 * @author joerg
 */
public final class CORELanguageRegistry {
    
    /**
     * Singleton instance variable.
     */
    private static CORELanguageRegistry instance;
    
    /**
     * List of CORE languages.
     */
    private Map<String, COREExternalLanguage> languages; 
    
    /**
     * Singleton pattern - initialize only a single registry.
     */
    private CORELanguageRegistry() {
        languages = new HashMap<String, COREExternalLanguage>();
    }
    
    /**
     * Function to get the language registry instance.
     * 
     * @return Returns the CORELanguageRegistry singleton.
     */
    public static CORELanguageRegistry getRegistry() {
        if (instance == null) {
            instance = new CORELanguageRegistry();
        }
        
        return instance;
    }
    
    /**
     * Registers languages supported by CORE.
     * 
     * @param language the language to be registered
     */
    public void registerLanguage(COREExternalLanguage language) {
        try {
         
            // Register resource factories of the language.
            Class<?> factoryClass = Class.forName(language.getResourceFactory());
            Factory factory = (Factory) factoryClass.newInstance();
            ResourceManager.registerExtensionFactory(language.getFileExtension(), factory);
            
            // Initialize adapter of the language
            @SuppressWarnings("unchecked")
            Class<? extends AdapterFactory> adapterClass =
                    (Class<? extends AdapterFactory>) Class.forName(language.getAdapterFactory());
            AdapterFactoryRegistry.INSTANCE.addAdapterFactory(adapterClass);
            
            // Register the language weaver
            String weaverName = language.getWeaverClassName();
            if (weaverName != null && !weaverName.isEmpty()) {
                Class<?> weaver = Class.forName(weaverName);
                COREModelWeaver<?> modelWeaver = (COREModelWeaver<?>) weaver.newInstance();
                COREWeaver.getInstance().registerCOREModelWeaver(language.getName(), modelWeaver);
            }
            
            languages.put(language.getName(), language);
            // TODO: figure out whether we want to use file extensions or full name to refer to a language
            languages.put(language.getFileExtension(), language);
        //CHECKSTYLE:IGNORE IllegalCatch: A lot of different exceptions can be thrown.
        } catch (Exception e) {
            System.err.format("Language registration for '%s' failed: %s\n", language.getName(), e.toString());
        }
    }

    /**
     * Return the list of registered languages.
     * 
     * @return the language
     */
    public Collection<COREExternalLanguage> getLanguages() {
        return languages.values();
    }

    /**
     * Return the CORELanguage corresponding to the String name or else null.
     * 
     * @param name the name of the language 
     * @return the CORELanguage with the name name
     */
    public COREExternalLanguage getLanguage(String name) {
        return languages.get(name);
    }
    
}
