package ca.mcgill.sel.core.perspective;

public class PerspectiveName {
    
    public static final String DOMAIN_MODEL_PERSPECTIVE = "Domain Model Perspective";
    public static final String DESIGN_MODEL_PERSPECTIVE = "Design Model Perspective";

}
