package ca.mcgill.sel.core.perspective;

import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREReexposeAction;

/**
 * The controller for {@link COREPerspective}s.
 *
 * @author hyacinthali
 */
public class ActionValidator {
    /**
     * Creates a new instance of {@link ActionValidator}.
     */
    public ActionValidator() {
    }
    
    /*
     * COREPerspective Actions Validation Methods for UML Class Diagram Language
     */
    
    public boolean canCreateNewClass(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_CLASS.getName()) ) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean canCreateImplementationClass(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_IMPLEMENTATION_CLASS.getName()) ) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean canRemoveClassifier(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.DELETE_CLASS.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canEditClassifier(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.EDIT_CLASS.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canCreateAssociation(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_ASSOCIATION.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canRemoveNote(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.DELETE_NOTE.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canCreateEnum(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_ENUM.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }

    public boolean canCreateNewNote(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_NOTE.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }

    public boolean canCreateNaryAssociation(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_NARY_ASSOCIATION.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canEditAssociation(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.EDIT_ASSOCIATION.getName()) ) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean canDeleteAssociation(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.DELETE_ASSOCIATION.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    } 
    
    public boolean canEditClass(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.EDIT_CLASS.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canEditVisibility(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.EDIT_VISIBILITY.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canEditStatic(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.EDIT_STATIC.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }

    public boolean canRemoveAttribute(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.DELETE_ATTRIBUTE.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }

    public boolean canCreateAttribute(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_ATTRIBUTE.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }

    public boolean canEditOperation(COREPerspective perspective) {
        
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.EDIT_OPERATION.getName()) ) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean canRemoveOperation(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.REMOVE_OPERATION.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canCreateOperation(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_OPERATION.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
      
    public boolean canCreateParameter(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.ADD_OPERATION_PARAMETER.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canEditEnum(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.EDIT_ENUM.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public boolean canDeleteEnum(COREPerspective perspective) {
        
        for (COREAction pAction : perspective.getActions()) {
            if (pAction instanceof COREReexposeAction) {
                COREReexposeAction action = (COREReexposeAction) pAction;
                String actionName = action.getReexposedAction().getName();
                if (actionName.equals(PerspectiveAction.DELETE_ENUM.getName()) ) {
                    return true;
                }
            }
        }
        
        return false;
    }

}
