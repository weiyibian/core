package ca.mcgill.sel.core.perspective;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREReexposeAction;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.language.registry.CORELanguageRegistry;
import ca.mcgill.sel.core.util.COREArtefactUtil;

/**
 * Helper class with convenient methods for working with CORE Perspective.
 * 
 * @author Hyacinth
 */
public final class COREPerspectiveUtil {
    
    /**
     * Singleton instance variable.
     */
    public static COREPerspectiveUtil INSTANCE = new COREPerspectiveUtil();
    
    /**
     * Map between a perspective and its controller action
     */
    // private Map<COREPerspective, EObject> perspectiveControllers;
    
    /**
     * List of CORE perspectives.
     */
    private Map<String, COREPerspective> perspectives; 
    
    /**
     * Singleton pattern - initialize only a single instance.
     */
    private COREPerspectiveUtil() {
        perspectives = new HashMap<String, COREPerspective>();
        // perspectiveControllers = new HashMap<COREPerspective, EObject>();
    }
    
//    /**
//     * Function to get the perspective util instance.
//     * 
//     * @return Returns the COREPerspective singleton.
//     */
//    public static COREPerspectiveUtil getInstance() {
//        if (instance == null) {
//            instance = new COREPerspectiveUtil();
//        }
//        
//        return instance;
//    }
    
//    /**
//     * Maps each perspective with a corresponding controller
//     */
//    public void mapPerspectiveController() {
//        Collection<COREPerspective> perspectives = COREPerspectiveUtil.INSTANCE.getPerspectives();
//        for (COREPerspective p : perspectives) {
//            if (p.getName().equals("Design Model Perspective")) {
//                perspectiveControllers.put(p, (EObject) DesignModelController.INSTANCE);
//            } else if (p.getName().equals("Domain Model Perspective")) {
//                perspectiveControllers.put(p, (EObject) DomainModelController.INSTANCE);
//            }
//        }
//    }
//    
//    
//
//    /**
//     * @return the perspectiveControllers
//     */
//    public Map<COREPerspective, EObject> getPerspectiveControllers() {
//        return perspectiveControllers;
//    }
//    
//    /**
//     * Gets a perspective controller
//     */
//    public EObject getPerspectiveController(COREPerspective perspective) {
//        return perspectiveControllers.get(perspective);
//    }

//    /**
//     * Associate domain model perspective with a class diagram
//     */
//    public void associateDomainPerspective() {
//        COREPerspective domainPerspective = COREPerspectiveUtil.INSTANCE.getPerspective("Domain Model Perspective");
//        List<COREExternalLanguage> languages = new ArrayList<COREExternalLanguage>();
//        COREExternalLanguage language = CORELanguageRegistry.getRegistry().getLanguage("UML Class Diagram");
//        languages.add(language);
//        if (domainPerspective != null) {
//            COREPerspectiveUtil.INSTANCE.addPerspectiveLanguage(domainPerspective, languages);
//        }
//    }
//    
//    /**
//     * Associate design model perspective with a class diagram
//     */
//    public void associateDesignPerspective() {
//        COREPerspective domainPerspective = COREPerspectiveUtil.INSTANCE.getPerspective("Design Model Perspective");
//        List<COREExternalLanguage> languages = new ArrayList<COREExternalLanguage>();
//        COREExternalLanguage language = CORELanguageRegistry.getRegistry().getLanguage("UML Class Diagram");
//        languages.add(language);
//        if (domainPerspective != null) {
//            COREPerspectiveUtil.INSTANCE.addPerspectiveLanguage(domainPerspective, languages);
//        }
//    }
    
    /**
     * Associates a perspective with given existing language(s).
     * 
     * @param perspective the perspective to be associated with language(s)
     * @param languages to associate with a perspective
     */
//    public void addPerspectiveLanguage(COREPerspective perspective, List<COREExternalLanguage> languages) {
//        for (COREExternalLanguage l : languages) {
//            perspective.getLanguages().add(l);
//            associateReexposedActions(perspective, l);
//        }
//    }
    
    /**
     * Associates the reexpose actions of the perspective with the corresponding language actions
     * TODO this needs to be changed when dealing with multi-language perspective
     * @param perspective the perspective
     */
    private void associateReexposedActions(COREPerspective perspective) {
        for (CORELanguage language : perspective.getLanguages().values()) {
            for (COREAction pAction : perspective.getActions()) {
                if (pAction instanceof COREReexposeAction) {
                    for (COREAction lAction : language.getActions()) {
                        //TODO currently only re-exposed actions are supported.
                        COREReexposeAction a = (COREReexposeAction) pAction;
                        if (a.getName().equals(lAction.getName())) {
                            a.setReexposedAction(lAction);
                        }
                    }
                }
            }
        }
    }

    /**
     * Return the list of existing perspectives.
     * 
     * @return the perspective
     */
    public Collection<COREPerspective> getPerspectives() {
        return perspectives.values();
    }

    /**
     * Return the COREPerspective corresponding to the String name or else null.
     * 
     * @param name the name of the language 
     * @return the COREPerspective with the name name
     */
    public COREPerspective getPerspective(String name) {
        return perspectives.get(name);
    }
    
    /**
     * Retrieves the perspective {@link COREPerspective} from the {@link COREArtefactUtil's} 
     * @param model root model element of the external artefact
     * @return the perspective of the model 
     */
    public COREPerspective getModelPerspective(EObject model) {
        String perspectiveName = 
                COREArtefactUtil.getReferencingExternalArtefact(model).getScene().getPerspectiveName();
        return perspectives.get(perspectiveName);
    }

    
    /**
    * Loads pre-defined perspectives.
    */
    public void loadPerspectives() {
       List<String> ps = ResourceUtil.getResourceListing("models/perspectives/", ".core");
       if (ps != null) {
           for (String p : ps) {
               
               // load existing perspectives                
               URI fileURI = URI.createURI(p);
               COREConcern languageConcern = (COREConcern) ResourceManager.loadModel(fileURI); 
               for (COREArtefact a : languageConcern.getArtefacts()) {
                   if (a instanceof COREPerspective) {
                       COREPerspective existingPerspective = (COREPerspective) a;
                       perspectives.put(existingPerspective.getName(), existingPerspective);   
                       associateReexposedActions(existingPerspective);
                   }
               }
           }
       }
    }
   
    /**
     * @param c
     * @param roleName
     * @param artefact
     * @throws IOException
     */
    public void saveModel(COREConcern c, String roleName, COREExternalArtefact artefact) throws IOException {
       URI idealURI = c.eResource().getURI();       
       idealURI = idealURI.trimSegments(1);
       idealURI = idealURI.appendSegment(artefact.getLanguageName());
       
       char addOn = 'a' - 1;
       String fileRoleName = roleName.replace(' ', '_').toLowerCase();
       
       File suggestedFile;

       do {
           URI attempt;
           if (addOn == 'a' - 1) {
               attempt = idealURI.appendSegment(artefact.getName() + "." + fileRoleName);
           } else {
               attempt = idealURI.appendSegment(artefact.getName() + "-" + addOn + "." + fileRoleName);
           }
           String extension = CORELanguageRegistry.getRegistry().getLanguage(artefact.getLanguageName())
                   .getFileExtension();
           attempt = attempt.appendFileExtension(extension);
           suggestedFile = new File(attempt.toFileString());
           addOn++;
       } while (suggestedFile.exists());

       try {
           if (!suggestedFile.getParentFile().exists()) {
               // create parent directory if someone's trying to be tricky.
               suggestedFile.getParentFile().mkdirs();
           }
           suggestedFile.createNewFile();
       } catch (final IOException e) {
           // TODO display something to user
           e.printStackTrace();
       }

       ResourceManager.saveModel(artefact.getRootModelElement(), suggestedFile.getAbsolutePath());
    }

    /**
     * Operation that results all reuses associated with a feature for a given perspective. This includes any reuse
     * that has a model reuse attached to an artefact that is part of a scene that has the provided perspective type.
     * If the perspective parameter is null, then all reuses associated with any scene that is linked to the feature
     * are returned.
     *  
     * @param feature which feature we are interested in
     * @param perspective the perspective we consider (pass null to consider scenes from all perspectives)
     * @return a HashSet of reuses
     */
    public Collection<COREReuse> getReuses(COREFeature feature, COREPerspective perspective) {
       Collection<COREReuse> result = new HashSet<COREReuse>();
       
       Collection<COREScene> scenes = feature.getRealizedBy();       
       for (COREScene s : scenes) {
           if (perspective == null
                   || COREPerspectiveUtil.INSTANCE.getPerspective(s.getPerspectiveName()) == perspective) {
               ArrayList<COREArtefact> artefacts = COREArtefactUtil.getAllArtefactsOfScene(s);           
               for (COREArtefact a : artefacts) {
                   for (COREModelReuse mr : a.getModelReuses()) {
                       result.add(mr.getReuse());
                   }
                   
               }
           }
       }
       return result;
   }
   
}
