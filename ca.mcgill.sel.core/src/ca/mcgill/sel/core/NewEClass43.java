/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New EClass43</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.core.CorePackage#getNewEClass43()
 * @model
 * @generated
 */
public interface NewEClass43 extends EObject {
} // NewEClass43
