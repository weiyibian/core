/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nary Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.NaryElementMapping#getCardinalities <em>Cardinalities</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getNaryElementMapping()
 * @model
 * @generated
 */
public interface NaryElementMapping extends CORELanguageElementMapping {
    /**
     * Returns the value of the '<em><b>Cardinalities</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.ToCardinality}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.ToCardinality#getElementMapping <em>Element Mapping</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Cardinalities</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getNaryElementMapping_Cardinalities()
     * @see ca.mcgill.sel.core.ToCardinality#getElementMapping
     * @model opposite="elementMapping"
     * @generated
     */
    EList<ToCardinality> getCardinalities();

} // NaryElementMapping
