/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.SingleElementMapping#getTo <em>To</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getSingleElementMapping()
 * @model
 * @generated
 */
public interface SingleElementMapping extends COREElementMapping {
    /**
     * Returns the value of the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>To</em>' reference.
     * @see #setTo(CORELanguageElement)
     * @see ca.mcgill.sel.core.CorePackage#getSingleElementMapping_To()
     * @model required="true"
     * @generated
     */
    CORELanguageElement getTo();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.SingleElementMapping#getTo <em>To</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>To</em>' reference.
     * @see #getTo()
     * @generated
     */
    void setTo(CORELanguageElement value);

} // SingleElementMapping
