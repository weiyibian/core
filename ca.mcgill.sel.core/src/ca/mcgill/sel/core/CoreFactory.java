/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.core.CorePackage
 * @generated
 */
public interface CoreFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    CoreFactory eINSTANCE = ca.mcgill.sel.core.impl.CoreFactoryImpl.init();

    /**
     * Returns a new object of class '<em>CORE Impact Model</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Impact Model</em>'.
     * @generated
     */
    COREImpactModel createCOREImpactModel();

    /**
     * Returns a new object of class '<em>CORE Concern</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Concern</em>'.
     * @generated
     */
    COREConcern createCOREConcern();

    /**
     * Returns a new object of class '<em>CORE Feature</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Feature</em>'.
     * @generated
     */
    COREFeature createCOREFeature();

    /**
     * Returns a new object of class '<em>CORE Reuse</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Reuse</em>'.
     * @generated
     */
    COREReuse createCOREReuse();

    /**
     * Returns a new object of class '<em>CORE Impact Node</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Impact Node</em>'.
     * @generated
     */
    COREImpactNode createCOREImpactNode();

    /**
     * Returns a new object of class '<em>CORE Configuration</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Configuration</em>'.
     * @generated
     */
    COREConfiguration createCOREConfiguration();

    /**
     * Returns a new object of class '<em>CORE Feature Model</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Feature Model</em>'.
     * @generated
     */
    COREFeatureModel createCOREFeatureModel();

    /**
     * Returns a new object of class '<em>CORE Model Reuse</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Model Reuse</em>'.
     * @generated
     */
    COREModelReuse createCOREModelReuse();

    /**
     * Returns a new object of class '<em>CORE Contribution</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Contribution</em>'.
     * @generated
     */
    COREContribution createCOREContribution();

    /**
     * Returns a new object of class '<em>Layout Element</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Layout Element</em>'.
     * @generated
     */
    LayoutElement createLayoutElement();

    /**
     * Returns a new object of class '<em>CORE Feature Impact Node</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Feature Impact Node</em>'.
     * @generated
     */
    COREFeatureImpactNode createCOREFeatureImpactNode();

    /**
     * Returns a new object of class '<em>CORE Weighted Link</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Weighted Link</em>'.
     * @generated
     */
    COREWeightedLink createCOREWeightedLink();

    /**
     * Returns a new object of class '<em>CORE Model Extension</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Model Extension</em>'.
     * @generated
     */
    COREModelExtension createCOREModelExtension();

    /**
     * Returns a new object of class '<em>CORE Mapping</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Mapping</em>'.
     * @generated
     */
    <T> COREMapping<T> createCOREMapping();

    /**
     * Returns a new object of class '<em>CORE Scene</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Scene</em>'.
     * @generated
     */
    COREScene createCOREScene();

    /**
     * Returns a new object of class '<em>CORE Perspective</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Perspective</em>'.
     * @generated
     */
    COREPerspective createCOREPerspective();

    /**
     * Returns a new object of class '<em>CORE External Language</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE External Language</em>'.
     * @generated
     */
    COREExternalLanguage createCOREExternalLanguage();

    /**
     * Returns a new object of class '<em>CORE External Artefact</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE External Artefact</em>'.
     * @generated
     */
    COREExternalArtefact createCOREExternalArtefact();

    /**
     * Returns a new object of class '<em>COREUI Element</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>COREUI Element</em>'.
     * @generated
     */
    COREUIElement createCOREUIElement();

    /**
     * Returns a new object of class '<em>CORECI Element</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORECI Element</em>'.
     * @generated
     */
    CORECIElement createCORECIElement();

    /**
     * Returns a new object of class '<em>CORE Mapping Cardinality</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Mapping Cardinality</em>'.
     * @generated
     */
    COREMappingCardinality createCOREMappingCardinality();

    /**
     * Returns a new object of class '<em>CORE Reexpose Action</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Reexpose Action</em>'.
     * @generated
     */
    COREReexposeAction createCOREReexposeAction();

    /**
     * Returns a new object of class '<em>CORE Redefine Action</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Redefine Action</em>'.
     * @generated
     */
    CORERedefineAction createCORERedefineAction();

    /**
     * Returns a new object of class '<em>CORE Language Action</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Language Action</em>'.
     * @generated
     */
    CORELanguageAction createCORELanguageAction();

    /**
     * Returns a new object of class '<em>CORE Create Mapping</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Create Mapping</em>'.
     * @generated
     */
    CORECreateMapping createCORECreateMapping();

    /**
     * Returns a new object of class '<em>Binary Element Mapping</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Binary Element Mapping</em>'.
     * @generated
     */
    BinaryElementMapping createBinaryElementMapping();

    /**
     * Returns a new object of class '<em>Nary Element Mapping</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Nary Element Mapping</em>'.
     * @generated
     */
    NaryElementMapping createNaryElementMapping();

    /**
     * Returns a new object of class '<em>CORE New Action</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE New Action</em>'.
     * @generated
     */
    CORENewAction createCORENewAction();

    /**
     * Returns a new object of class '<em>CORE Model Element Mapping</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CORE Model Element Mapping</em>'.
     * @generated
     */
    COREModelElementMapping createCOREModelElementMapping();

    /**
     * Returns a new object of class '<em>To Cardinality</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>To Cardinality</em>'.
     * @generated
     */
    ToCardinality createToCardinality();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    CorePackage getCorePackage();

} //CoreFactory
