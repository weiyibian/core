/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Model Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREModelElementMapping#getFrom <em>From</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREModelElementMapping#getTo <em>To</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREModelElementMapping#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREModelElementMapping()
 * @model
 * @generated
 */
public interface COREModelElementMapping extends EObject {
    /**
     * Returns the value of the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>From</em>' reference.
     * @see #setFrom(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelElementMapping_From()
     * @model required="true"
     * @generated
     */
    EObject getFrom();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREModelElementMapping#getFrom <em>From</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>From</em>' reference.
     * @see #getFrom()
     * @generated
     */
    void setFrom(EObject value);

    /**
     * Returns the value of the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>To</em>' reference.
     * @see #setTo(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelElementMapping_To()
     * @model required="true"
     * @generated
     */
    EObject getTo();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREModelElementMapping#getTo <em>To</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>To</em>' reference.
     * @see #getTo()
     * @generated
     */
    void setTo(EObject value);

    /**
     * Returns the value of the '<em><b>Type</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getInstances <em>Instances</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' reference.
     * @see #setType(CORELanguageElementMapping)
     * @see ca.mcgill.sel.core.CorePackage#getCOREModelElementMapping_Type()
     * @see ca.mcgill.sel.core.CORELanguageElementMapping#getInstances
     * @model opposite="instances" required="true"
     * @generated
     */
    CORELanguageElementMapping getType();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREModelElementMapping#getType <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' reference.
     * @see #getType()
     * @generated
     */
    void setType(CORELanguageElementMapping value);

} // COREModelElementMapping
