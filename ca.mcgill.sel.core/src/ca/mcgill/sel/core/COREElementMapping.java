/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREElementMapping#getRelationship <em>Relationship</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREElementMapping#getFrom <em>From</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREElementMapping#getInstances <em>Instances</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREElementMapping#getFromCardinality <em>From Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREElementMapping#getToCardinality <em>To Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREElementMapping#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREElementMapping()
 * @model abstract="true"
 * @generated
 */
public interface COREElementMapping extends EObject {
    /**
     * Returns the value of the '<em><b>Relationship</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.CORERelationship}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Relationship</em>' attribute.
     * @see ca.mcgill.sel.core.CORERelationship
     * @see #setRelationship(CORERelationship)
     * @see ca.mcgill.sel.core.CorePackage#getCOREElementMapping_Relationship()
     * @model required="true"
     * @generated
     */
    CORERelationship getRelationship();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREElementMapping#getRelationship <em>Relationship</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Relationship</em>' attribute.
     * @see ca.mcgill.sel.core.CORERelationship
     * @see #getRelationship()
     * @generated
     */
    void setRelationship(CORERelationship value);

    /**
     * Returns the value of the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>From</em>' reference.
     * @see #setFrom(CORELanguageElement)
     * @see ca.mcgill.sel.core.CorePackage#getCOREElementMapping_From()
     * @model required="true"
     * @generated
     */
    CORELanguageElement getFrom();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREElementMapping#getFrom <em>From</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>From</em>' reference.
     * @see #getFrom()
     * @generated
     */
    void setFrom(CORELanguageElement value);

    /**
     * Returns the value of the '<em><b>Instances</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORECreateMapping}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORECreateMapping#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Instances</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREElementMapping_Instances()
     * @see ca.mcgill.sel.core.CORECreateMapping#getType
     * @model opposite="type"
     * @generated
     */
    EList<CORECreateMapping> getInstances();

    /**
     * Returns the value of the '<em><b>From Cardinality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.FromCardinality}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>From Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.FromCardinality
     * @see #setFromCardinality(FromCardinality)
     * @see ca.mcgill.sel.core.CorePackage#getCOREElementMapping_FromCardinality()
     * @model
     * @generated
     */
    FromCardinality getFromCardinality();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREElementMapping#getFromCardinality <em>From Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>From Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.FromCardinality
     * @see #getFromCardinality()
     * @generated
     */
    void setFromCardinality(FromCardinality value);

    /**
     * Returns the value of the '<em><b>To Cardinality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.ToCardinality}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>To Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.ToCardinality
     * @see #setToCardinality(ToCardinality)
     * @see ca.mcgill.sel.core.CorePackage#getCOREElementMapping_ToCardinality()
     * @model
     * @generated
     */
    ToCardinality getToCardinality();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREElementMapping#getToCardinality <em>To Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>To Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.ToCardinality
     * @see #getToCardinality()
     * @generated
     */
    void setToCardinality(ToCardinality value);

    /**
     * Returns the value of the '<em><b>Actions</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORERedefineAction}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORERedefineAction#getEMappings <em>EMappings</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Actions</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREElementMapping_Actions()
     * @see ca.mcgill.sel.core.CORERedefineAction#getEMappings
     * @model opposite="eMappings"
     * @generated
     */
    EList<CORERedefineAction> getActions();

} // COREElementMapping
