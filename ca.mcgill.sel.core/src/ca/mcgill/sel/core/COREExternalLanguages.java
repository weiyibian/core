/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE External Languages</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguages#getNsURI <em>Ns URI</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguages#getResourceFactory <em>Resource Factory</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguages#getAdapterFactory <em>Adapter Factory</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguages#getWeaverClassName <em>Weaver Class Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguages#getFileExtension <em>File Extension</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguages#getActions <em>Actions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREExternalLanguages#getLanguageElements <em>Language Elements</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguages()
 * @model
 * @generated
 */
public interface COREExternalLanguages extends CORELanguages {
    /**
     * Returns the value of the '<em><b>Ns URI</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Ns URI</em>' attribute.
     * @see #setNsURI(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguages_NsURI()
     * @model required="true"
     * @generated
     */
    String getNsURI();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguages#getNsURI <em>Ns URI</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Ns URI</em>' attribute.
     * @see #getNsURI()
     * @generated
     */
    void setNsURI(String value);

    /**
     * Returns the value of the '<em><b>Resource Factory</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Resource Factory</em>' attribute.
     * @see #setResourceFactory(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguages_ResourceFactory()
     * @model required="true"
     * @generated
     */
    String getResourceFactory();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguages#getResourceFactory <em>Resource Factory</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource Factory</em>' attribute.
     * @see #getResourceFactory()
     * @generated
     */
    void setResourceFactory(String value);

    /**
     * Returns the value of the '<em><b>Adapter Factory</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Adapter Factory</em>' attribute.
     * @see #setAdapterFactory(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguages_AdapterFactory()
     * @model required="true"
     * @generated
     */
    String getAdapterFactory();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguages#getAdapterFactory <em>Adapter Factory</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Adapter Factory</em>' attribute.
     * @see #getAdapterFactory()
     * @generated
     */
    void setAdapterFactory(String value);

    /**
     * Returns the value of the '<em><b>Weaver Class Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Weaver Class Name</em>' attribute.
     * @see #setWeaverClassName(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguages_WeaverClassName()
     * @model required="true"
     * @generated
     */
    String getWeaverClassName();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguages#getWeaverClassName <em>Weaver Class Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Weaver Class Name</em>' attribute.
     * @see #getWeaverClassName()
     * @generated
     */
    void setWeaverClassName(String value);

    /**
     * Returns the value of the '<em><b>File Extension</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>File Extension</em>' attribute.
     * @see #setFileExtension(String)
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguages_FileExtension()
     * @model required="true"
     * @generated
     */
    String getFileExtension();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREExternalLanguages#getFileExtension <em>File Extension</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>File Extension</em>' attribute.
     * @see #getFileExtension()
     * @generated
     */
    void setFileExtension(String value);

    /**
     * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORELanguageAction}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Actions</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguages_Actions()
     * @model containment="true" required="true"
     * @generated
     */
    EList<CORELanguageAction> getActions();

    /**
     * Returns the value of the '<em><b>Language Elements</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORELanguageElement}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Language Elements</em>' containment reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREExternalLanguages_LanguageElements()
     * @model containment="true"
     * @generated
     */
    EList<CORELanguageElement> getLanguageElements();

} // COREExternalLanguages
