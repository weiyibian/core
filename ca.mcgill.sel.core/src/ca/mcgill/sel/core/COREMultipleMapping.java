/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Multiple Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.COREMultipleMapping#getTo <em>To</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.COREMultipleMapping#isSingleInstance <em>Single Instance</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREMultipleMapping()
 * @model
 * @generated
 */
public interface COREMultipleMapping extends COREElementMapping {
    /**
     * Returns the value of the '<em><b>To</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORELanguageElement}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>To</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCOREMultipleMapping_To()
     * @model required="true"
     * @generated
     */
    EList<CORELanguageElement> getTo();

    /**
     * Returns the value of the '<em><b>Single Instance</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Single Instance</em>' attribute.
     * @see #setSingleInstance(boolean)
     * @see ca.mcgill.sel.core.CorePackage#getCOREMultipleMapping_SingleInstance()
     * @model
     * @generated
     */
    boolean isSingleInstance();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.COREMultipleMapping#isSingleInstance <em>Single Instance</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Single Instance</em>' attribute.
     * @see #isSingleInstance()
     * @generated
     */
    void setSingleInstance(boolean value);

} // COREMultipleMapping
