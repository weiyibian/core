/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Perspective Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREPerspectiveAction()
 * @model abstract="true"
 * @generated
 */
public interface COREPerspectiveAction extends COREAction {
} // COREPerspectiveAction
