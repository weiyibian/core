/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.BinaryElementMapping#getToCardinality <em>To Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.BinaryElementMapping#getTo <em>To</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getBinaryElementMapping()
 * @model
 * @generated
 */
public interface BinaryElementMapping extends CORELanguageElementMapping {
    /**
     * Returns the value of the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>To</em>' reference.
     * @see #setTo(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getBinaryElementMapping_To()
     * @model required="true"
     * @generated
     */
    EObject getTo();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.BinaryElementMapping#getTo <em>To</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>To</em>' reference.
     * @see #getTo()
     * @generated
     */
    void setTo(EObject value);

    /**
     * Returns the value of the '<em><b>To Cardinality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.Cardinality}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>To Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.Cardinality
     * @see #setToCardinality(Cardinality)
     * @see ca.mcgill.sel.core.CorePackage#getBinaryElementMapping_ToCardinality()
     * @model required="true"
     * @generated
     */
    Cardinality getToCardinality();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.BinaryElementMapping#getToCardinality <em>To Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>To Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.Cardinality
     * @see #getToCardinality()
     * @generated
     */
    void setToCardinality(Cardinality value);

} // BinaryElementMapping
