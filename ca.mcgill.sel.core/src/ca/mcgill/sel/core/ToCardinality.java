/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>To Cardinality</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.ToCardinality#getToCardinality <em>To Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.ToCardinality#getElementMapping <em>Element Mapping</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.ToCardinality#getLanguageElement <em>Language Element</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getToCardinality()
 * @model
 * @generated
 */
public interface ToCardinality extends EObject {
    /**
     * Returns the value of the '<em><b>To Cardinality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.Cardinality}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>To Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.Cardinality
     * @see #setToCardinality(Cardinality)
     * @see ca.mcgill.sel.core.CorePackage#getToCardinality_ToCardinality()
     * @model required="true"
     * @generated
     */
    Cardinality getToCardinality();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.ToCardinality#getToCardinality <em>To Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>To Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.Cardinality
     * @see #getToCardinality()
     * @generated
     */
    void setToCardinality(Cardinality value);

    /**
     * Returns the value of the '<em><b>Element Mapping</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.NaryElementMapping#getCardinalities <em>Cardinalities</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Element Mapping</em>' reference.
     * @see #setElementMapping(NaryElementMapping)
     * @see ca.mcgill.sel.core.CorePackage#getToCardinality_ElementMapping()
     * @see ca.mcgill.sel.core.NaryElementMapping#getCardinalities
     * @model opposite="cardinalities" required="true"
     * @generated
     */
    NaryElementMapping getElementMapping();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.ToCardinality#getElementMapping <em>Element Mapping</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Element Mapping</em>' reference.
     * @see #getElementMapping()
     * @generated
     */
    void setElementMapping(NaryElementMapping value);

    /**
     * Returns the value of the '<em><b>Language Element</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Language Element</em>' reference.
     * @see #setLanguageElement(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getToCardinality_LanguageElement()
     * @model required="true"
     * @generated
     */
    EObject getLanguageElement();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.ToCardinality#getLanguageElement <em>Language Element</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Language Element</em>' reference.
     * @see #getLanguageElement()
     * @generated
     */
    void setLanguageElement(EObject value);

} // ToCardinality
