/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Language Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getRelationship <em>Relationship</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getActions <em>Actions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getFromCardinality <em>From Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getInstances <em>Instances</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElementMapping#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping()
 * @model abstract="true"
 * @generated
 */
public interface CORELanguageElementMapping extends EObject {
    /**
     * Returns the value of the '<em><b>Relationship</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.CORERelationship}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Relationship</em>' attribute.
     * @see ca.mcgill.sel.core.CORERelationship
     * @see #setRelationship(CORERelationship)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_Relationship()
     * @model required="true"
     * @generated
     */
    CORERelationship getRelationship();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getRelationship <em>Relationship</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Relationship</em>' attribute.
     * @see ca.mcgill.sel.core.CORERelationship
     * @see #getRelationship()
     * @generated
     */
    void setRelationship(CORERelationship value);

    /**
     * Returns the value of the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>From</em>' reference.
     * @see #setFrom(EObject)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_From()
     * @model required="true"
     * @generated
     */
    EObject getFrom();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getFrom <em>From</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>From</em>' reference.
     * @see #getFrom()
     * @generated
     */
    void setFrom(EObject value);

    /**
     * Returns the value of the '<em><b>Actions</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.CORECreateMapping}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.CORECreateMapping#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Actions</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_Actions()
     * @see ca.mcgill.sel.core.CORECreateMapping#getType
     * @model opposite="type"
     * @generated
     */
    EList<CORECreateMapping> getActions();

    /**
     * Returns the value of the '<em><b>From Cardinality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.Cardinality}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>From Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.Cardinality
     * @see #setFromCardinality(Cardinality)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_FromCardinality()
     * @model required="true"
     * @generated
     */
    Cardinality getFromCardinality();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElementMapping#getFromCardinality <em>From Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>From Cardinality</em>' attribute.
     * @see ca.mcgill.sel.core.Cardinality
     * @see #getFromCardinality()
     * @generated
     */
    void setFromCardinality(Cardinality value);

    /**
     * Returns the value of the '<em><b>Instances</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREModelElementMapping}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREModelElementMapping#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Instances</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElementMapping_Instances()
     * @see ca.mcgill.sel.core.COREModelElementMapping#getType
     * @model opposite="type"
     * @generated
     */
    EList<COREModelElementMapping> getInstances();

} // CORELanguageElementMapping
