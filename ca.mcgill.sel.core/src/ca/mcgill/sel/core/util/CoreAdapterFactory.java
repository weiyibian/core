/**
 */
package ca.mcgill.sel.core.util;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.BinaryElementMapping;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.CORECreateMapping;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.CORENewAction;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREPerspectiveAction;
import ca.mcgill.sel.core.CORERedefineAction;
import ca.mcgill.sel.core.COREReexposeAction;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.COREUIElement;
import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.LayoutElement;
import ca.mcgill.sel.core.NaryElementMapping;
import ca.mcgill.sel.core.ToCardinality;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.core.CorePackage
 * @generated
 */
public class CoreAdapterFactory extends AdapterFactoryImpl {
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static CorePackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CoreAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = CorePackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CoreSwitch<Adapter> modelSwitch =
        new CoreSwitch<Adapter>() {
            @Override
            public Adapter caseCOREArtefact(COREArtefact object) {
                return createCOREArtefactAdapter();
            }
            @Override
            public Adapter caseCOREImpactModel(COREImpactModel object) {
                return createCOREImpactModelAdapter();
            }
            @Override
            public Adapter caseCOREConcern(COREConcern object) {
                return createCOREConcernAdapter();
            }
            @Override
            public Adapter caseCOREFeature(COREFeature object) {
                return createCOREFeatureAdapter();
            }
            @Override
            public Adapter caseCOREModelComposition(COREModelComposition object) {
                return createCOREModelCompositionAdapter();
            }
            @Override
            public <T> Adapter caseCORELink(CORELink<T> object) {
                return createCORELinkAdapter();
            }
            @Override
            public Adapter caseCORENamedElement(CORENamedElement object) {
                return createCORENamedElementAdapter();
            }
            @Override
            public Adapter caseCOREReuse(COREReuse object) {
                return createCOREReuseAdapter();
            }
            @Override
            public Adapter caseCOREImpactNode(COREImpactNode object) {
                return createCOREImpactNodeAdapter();
            }
            @Override
            public Adapter caseCOREConfiguration(COREConfiguration object) {
                return createCOREConfigurationAdapter();
            }
            @Override
            public Adapter caseCOREFeatureModel(COREFeatureModel object) {
                return createCOREFeatureModelAdapter();
            }
            @Override
            public Adapter caseCOREModelReuse(COREModelReuse object) {
                return createCOREModelReuseAdapter();
            }
            @Override
            public Adapter caseCOREContribution(COREContribution object) {
                return createCOREContributionAdapter();
            }
            @Override
            public Adapter caseLayoutMap(Map.Entry<EObject, LayoutElement> object) {
                return createLayoutMapAdapter();
            }
            @Override
            public Adapter caseLayoutElement(LayoutElement object) {
                return createLayoutElementAdapter();
            }
            @Override
            public Adapter caseLayoutContainerMap(Map.Entry<EObject, EMap<EObject, LayoutElement>> object) {
                return createLayoutContainerMapAdapter();
            }
            @Override
            public Adapter caseCOREFeatureImpactNode(COREFeatureImpactNode object) {
                return createCOREFeatureImpactNodeAdapter();
            }
            @Override
            public <T> Adapter caseCOREModelElementComposition(COREModelElementComposition<T> object) {
                return createCOREModelElementCompositionAdapter();
            }
            @Override
            public Adapter caseCOREWeightedLink(COREWeightedLink object) {
                return createCOREWeightedLinkAdapter();
            }
            @Override
            public Adapter caseCOREModelExtension(COREModelExtension object) {
                return createCOREModelExtensionAdapter();
            }
            @Override
            public <T> Adapter caseCOREMapping(COREMapping<T> object) {
                return createCOREMappingAdapter();
            }
            @Override
            public Adapter caseCOREScene(COREScene object) {
                return createCORESceneAdapter();
            }
            @Override
            public Adapter caseCOREPerspective(COREPerspective object) {
                return createCOREPerspectiveAdapter();
            }
            @Override
            public Adapter caseCOREExternalLanguage(COREExternalLanguage object) {
                return createCOREExternalLanguageAdapter();
            }
            @Override
            public Adapter caseCOREPerspectiveAction(COREPerspectiveAction object) {
                return createCOREPerspectiveActionAdapter();
            }
            @Override
            public Adapter caseCOREExternalArtefact(COREExternalArtefact object) {
                return createCOREExternalArtefactAdapter();
            }
            @Override
            public Adapter caseCOREUIElement(COREUIElement object) {
                return createCOREUIElementAdapter();
            }
            @Override
            public Adapter caseCORECIElement(CORECIElement object) {
                return createCORECIElementAdapter();
            }
            @Override
            public Adapter caseCOREMappingCardinality(COREMappingCardinality object) {
                return createCOREMappingCardinalityAdapter();
            }
            @Override
            public Adapter caseCORELanguageElementMapping(CORELanguageElementMapping object) {
                return createCORELanguageElementMappingAdapter();
            }
            @Override
            public Adapter caseCOREReexposeAction(COREReexposeAction object) {
                return createCOREReexposeActionAdapter();
            }
            @Override
            public Adapter caseCORERedefineAction(CORERedefineAction object) {
                return createCORERedefineActionAdapter();
            }
            @Override
            public Adapter caseCORELanguageAction(CORELanguageAction object) {
                return createCORELanguageActionAdapter();
            }
            @Override
            public Adapter caseCORECreateMapping(CORECreateMapping object) {
                return createCORECreateMappingAdapter();
            }
            @Override
            public Adapter caseBinaryElementMapping(BinaryElementMapping object) {
                return createBinaryElementMappingAdapter();
            }
            @Override
            public Adapter caseNaryElementMapping(NaryElementMapping object) {
                return createNaryElementMappingAdapter();
            }
            @Override
            public Adapter caseCORELanguage(CORELanguage object) {
                return createCORELanguageAdapter();
            }
            @Override
            public Adapter caseCOREAction(COREAction object) {
                return createCOREActionAdapter();
            }
            @Override
            public Adapter caseCORENewAction(CORENewAction object) {
                return createCORENewActionAdapter();
            }
            @Override
            public Adapter caseCOREModelElementMapping(COREModelElementMapping object) {
                return createCOREModelElementMappingAdapter();
            }
            @Override
            public Adapter caseToCardinality(ToCardinality object) {
                return createToCardinalityAdapter();
            }
            @Override
            public Adapter caseLanguageMap(Map.Entry<String, CORELanguage> object) {
                return createLanguageMapAdapter();
            }
            @Override
            public Adapter caseArtefactMap(Map.Entry<String, EList<COREArtefact>> object) {
                return createArtefactMapAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object) {
                return createEObjectAdapter();
            }
        };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREArtefact <em>CORE Artefact</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREArtefact
     * @generated
     */
    public Adapter createCOREArtefactAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREImpactModel <em>CORE Impact Model</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREImpactModel
     * @generated
     */
    public Adapter createCOREImpactModelAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREConcern <em>CORE Concern</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREConcern
     * @generated
     */
    public Adapter createCOREConcernAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREFeature <em>CORE Feature</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREFeature
     * @generated
     */
    public Adapter createCOREFeatureAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREModelComposition <em>CORE Model Composition</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREModelComposition
     * @generated
     */
    public Adapter createCOREModelCompositionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORELink <em>CORE Link</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORELink
     * @generated
     */
    public Adapter createCORELinkAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREMapping <em>CORE Mapping</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREMapping
     * @generated
     */
    public Adapter createCOREMappingAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREScene <em>CORE Scene</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREScene
     * @generated
     */
    public Adapter createCORESceneAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREPerspective <em>CORE Perspective</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREPerspective
     * @generated
     */
    public Adapter createCOREPerspectiveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREExternalLanguage <em>CORE External Language</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREExternalLanguage
     * @generated
     */
    public Adapter createCOREExternalLanguageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORELanguage <em>CORE Language</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORELanguage
     * @generated
     */
    public Adapter createCORELanguageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREAction <em>CORE Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREAction
     * @generated
     */
    public Adapter createCOREActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORENewAction <em>CORE New Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORENewAction
     * @generated
     */
    public Adapter createCORENewActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREModelElementMapping <em>CORE Model Element Mapping</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREModelElementMapping
     * @generated
     */
    public Adapter createCOREModelElementMappingAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.ToCardinality <em>To Cardinality</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.ToCardinality
     * @generated
     */
    public Adapter createToCardinalityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Language Map</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
    public Adapter createLanguageMapAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Artefact Map</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
    public Adapter createArtefactMapAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREPerspectiveAction <em>CORE Perspective Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREPerspectiveAction
     * @generated
     */
    public Adapter createCOREPerspectiveActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREExternalArtefact <em>CORE External Artefact</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREExternalArtefact
     * @generated
     */
    public Adapter createCOREExternalArtefactAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREUIElement <em>COREUI Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREUIElement
     * @generated
     */
    public Adapter createCOREUIElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORECIElement <em>CORECI Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORECIElement
     * @generated
     */
    public Adapter createCORECIElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREMappingCardinality <em>CORE Mapping Cardinality</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREMappingCardinality
     * @generated
     */
    public Adapter createCOREMappingCardinalityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORELanguageElementMapping <em>CORE Language Element Mapping</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORELanguageElementMapping
     * @generated
     */
    public Adapter createCORELanguageElementMappingAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREReexposeAction <em>CORE Reexpose Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREReexposeAction
     * @generated
     */
    public Adapter createCOREReexposeActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORERedefineAction <em>CORE Redefine Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORERedefineAction
     * @generated
     */
    public Adapter createCORERedefineActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORELanguageAction <em>CORE Language Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORELanguageAction
     * @generated
     */
    public Adapter createCORELanguageActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORECreateMapping <em>CORE Create Mapping</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORECreateMapping
     * @generated
     */
    public Adapter createCORECreateMappingAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.BinaryElementMapping <em>Binary Element Mapping</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.BinaryElementMapping
     * @generated
     */
    public Adapter createBinaryElementMappingAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.NaryElementMapping <em>Nary Element Mapping</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.NaryElementMapping
     * @generated
     */
    public Adapter createNaryElementMappingAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.CORENamedElement <em>CORE Named Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.CORENamedElement
     * @generated
     */
    public Adapter createCORENamedElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREReuse <em>CORE Reuse</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREReuse
     * @generated
     */
    public Adapter createCOREReuseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREImpactNode <em>CORE Impact Node</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREImpactNode
     * @generated
     */
    public Adapter createCOREImpactNodeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREConfiguration <em>CORE Configuration</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREConfiguration
     * @generated
     */
    public Adapter createCOREConfigurationAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREFeatureModel <em>CORE Feature Model</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREFeatureModel
     * @generated
     */
    public Adapter createCOREFeatureModelAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREModelReuse <em>CORE Model Reuse</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREModelReuse
     * @generated
     */
    public Adapter createCOREModelReuseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREContribution <em>CORE Contribution</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREContribution
     * @generated
     */
    public Adapter createCOREContributionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Layout Map</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
    public Adapter createLayoutMapAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.LayoutElement <em>Layout Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.LayoutElement
     * @generated
     */
    public Adapter createLayoutElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Layout Container Map</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
    public Adapter createLayoutContainerMapAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREFeatureImpactNode <em>CORE Feature Impact Node</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREFeatureImpactNode
     * @generated
     */
    public Adapter createCOREFeatureImpactNodeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREModelElementComposition <em>CORE Model Element Composition</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREModelElementComposition
     * @generated
     */
    public Adapter createCOREModelElementCompositionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREWeightedLink <em>CORE Weighted Link</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREWeightedLink
     * @generated
     */
    public Adapter createCOREWeightedLinkAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link ca.mcgill.sel.core.COREModelExtension <em>CORE Model Extension</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see ca.mcgill.sel.core.COREModelExtension
     * @generated
     */
    public Adapter createCOREModelExtensionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} //CoreAdapterFactory
