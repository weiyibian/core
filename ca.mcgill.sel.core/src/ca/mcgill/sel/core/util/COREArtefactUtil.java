package ca.mcgill.sel.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.COREUIElement;
import ca.mcgill.sel.core.CorePackage;

/**
 * Helper class with convenient static methods for working with CORE Artefacts.
 *
 * @author mschoettle
 * @author joerg
 */
public final class COREArtefactUtil {

    /**
     * Creates a new instance of {@link COREArtefactUtil}.
     */
    private COREArtefactUtil() {
        // suppress default constructor
    }
      
    /**
     * This operation will look in all external artefacts currently loaded in memory whether it can find
     * one that refers to the model that contains the objectOfInterest.
     * 
     * @param objectOfInterest the object that is part of a model that is being referenced by a
     * {@link COREExternalArtefact}
     * @return the {@link COREExternalArtefact} referencing the root of the model containing the object
     * of interest
     */
    public static COREExternalArtefact getReferencingExternalArtefact(EObject objectOfInterest) {
        if (objectOfInterest instanceof COREExternalArtefact) {
            return (COREExternalArtefact) objectOfInterest;
        }
        EObject rootContainer = EcoreUtil.getRootContainer(objectOfInterest);
        EList<Resource> resources = ResourceManager.getResourceSet().getResources();

        // look in all the external artefacts contained in the concern
        ArrayList<Resource> resourceListCopy = new ArrayList<Resource>(resources);
        for (Resource resource : resourceListCopy) {
            EObject root = resource.getContents().get(0);
            if (root instanceof COREConcern) {
                COREConcern concern = (COREConcern) root;
                for (COREArtefact artifact : concern.getArtefacts()) {
                    if (artifact instanceof COREExternalArtefact 
                            && ((COREExternalArtefact) artifact).getRootModelElement()
                            == rootContainer) {
                        return (COREExternalArtefact) artifact;
                    }
                }
                for (COREArtefact artifact : concern.getTemporaryArtefacts()) {
                    if (artifact instanceof COREExternalArtefact 
                            && ((COREExternalArtefact) artifact).getRootModelElement()
                            == rootContainer) {
                        return (COREExternalArtefact) artifact;
                    }
                }                
            }
        }
        
        return null;
    }
    
    /**
     * Given a {@link COREMapping}, gather all the elements that recursively are mapped to the mapping.
     * 
     * @param mapping - The mapping
     * @return A set of {@link EObject} containing the elements mapped to the given {@link COREMapping}
     */
    public static Set<EObject> getAllMappedElements(COREMapping<? extends EObject> mapping) {
        // Get the from element of the mapping
        return getAllMappedElementsRecursif(mapping.getFrom());
    }
    
    /**
     * Check the {@COREModelComposition} associate to a given element to 
     * gather recursively all the elements that are mapped to it.
     * Return the element itself among the other results.
     * 
     * @param element - The element we want to gather the elements from
     * @return A set of {@link EObject} containing the elements mapped to the given element
     */
    private static Set<EObject> getAllMappedElementsRecursif(EObject element) {
        // Add our element
        Set<EObject> elementsToGather = new HashSet<>();
        elementsToGather.add(element);
        
        // Get all the mappings link to our element
        Set<COREModelComposition> compositions = new HashSet<>();
        compositions.addAll(COREModelUtil.collectAllModelCompositions(element));
        
        for (COREModelComposition modelComposition : compositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                @SuppressWarnings("unchecked")
                COREMapping<? extends EObject>  mappingFromComp = (COREMapping<? extends EObject>) composition;
                
                // If this mapping is linked to our element, repeat the function for the from element of this mapping
                if (mappingFromComp.getTo() == element) {
                    elementsToGather.addAll(getAllMappedElementsRecursif(mappingFromComp.getFrom()));
                }
            }
        }
        return elementsToGather;
    }
    
    /**
     * Check the {@COREModelComposition} associate to a given element to 
     * gather recursively all the elements that are mapped to it.
     * Return the element itself among the resulting set.
     * 
     * @param <T> the generic type parameter
     * @param element - The element we want to gather the elements from
     * @return A set of {@link EObject} containing the elements mapped to the given element
     */
    public static <T extends EObject> Set<T> getAllMappedElementsRecursiveOfType(T element) {
        // Add our element
        Set<T> elementsToGather = new HashSet<T>();
        elementsToGather.add(element);
        
        // Get all the mappings link to our element
        Set<COREModelComposition> compositions = new HashSet<>();
        compositions.addAll(COREModelUtil.collectAllModelCompositions(element));
        
        for (COREModelComposition modelComposition : compositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                @SuppressWarnings("unchecked")
                COREMapping<? extends EObject>  mappingFromComp = (COREMapping<? extends EObject>) composition;
                
                // If this mapping is linked to our element, repeat the function for the from element of this mapping
                if (mappingFromComp.getTo() == element) {
                    @SuppressWarnings("unchecked")
                    Set<T> additionalElements = (Set<T>) getAllMappedElementsRecursiveOfType(mappingFromComp.getFrom());
                    elementsToGather.addAll(additionalElements);
                }
            }
        }
        return elementsToGather;
    }
    
    /**
     * Gather a {@link CORECIElement} from its {@link COREMappingCardinality}.
     * 
     * @param mappingCardinality the {@link COREMappingCardinality}
     * @param root the elements root
     * @return the {@link CORECIElement}
     */
    public static CORECIElement gatherCIElementFromMappingCardinality(COREMappingCardinality mappingCardinality,
            COREExternalArtefact root) {
        Collection<COREExternalArtefact> artefacts = COREModelUtil.getExtendedAndReusedArtefacts(root);
        artefacts.add(root);
        for (COREExternalArtefact artefact : artefacts) {
            for (CORECIElement ciElement : artefact.getCiElements()) {
                if (ciElement.getMappingCardinality() == mappingCardinality) {
                    return ciElement;
                }
            }
        }
        return null;
    }
    
    /**
     * Return the next set of possible reference mapping for a given mapping.
     * 
     * @param element - The element that will be mapped
     * @return A collection of the mappings, null if that couldn't be done
     */
    public static Collection<COREMapping<? extends EObject>> getNextReferencedMappings(EObject element) {
        CORECIElement ciElement = getCIElementFor(element);
        if (ciElement == null) {
            return null;
        }
        
        COREExternalArtefact root = getReferencingExternalArtefact(ciElement.getModelElement());
        Collection<COREMapping<? extends EObject>> mappingsResult = new ArrayList<>();
        List<COREMappingCardinality> cardinalityReferences = ciElement.getReferenceCardinality();
        
        if (cardinalityReferences.size() == 0) {
            return null;
        }
        if (cardinalityReferences.size() == 1) {
            for (COREMapping<? extends EObject> mapping
                    : getPossibleReferenceMappingsFromCardinalityRef(ciElement, cardinalityReferences.get(0))) {
                if (!isMappingAlreadyReferenced(mapping, root)) {
                    mappingsResult.add(mapping);
                    break;
                }
            }
        } else {
            // Retrieve all mappings possible by cardinality reference
            List<List<COREMapping<? extends EObject>>> mappingsByRef = new ArrayList<>();
            for (COREMappingCardinality cardinalityReference : cardinalityReferences) {
                List<COREMapping<? extends EObject>> mappings = new ArrayList<>();
                for (COREMapping<? extends EObject> mapping
                        : getPossibleReferenceMappingsFromCardinalityRef(ciElement, cardinalityReference)) {
                    mappings.add(mapping);
                }
                mappingsByRef.add(mappings);
            }
            
            Set<List<COREMapping<? extends EObject>>> allCombinations = getCombinations(mappingsByRef);
            
            for (List<COREMapping<? extends EObject>> combination : allCombinations) {
                if (!doReferencedMappingAlreadyExists(combination, root)) {
                    mappingsResult.addAll(combination);
                    break;
                }
            }
            
        }
        
        
        if (mappingsResult.size() == cardinalityReferences.size()) {
            return mappingsResult;
        }
        // There is not enough mappings available (should never occurs)
        return null;
    }
    
    /**
     * Given a list of mappings, check if all of them combined form an already existing referenced mapping.
     * 
     * @param mappings - The mappings
     * @param root - The model root
     * @return true if the referenced mapping already exists
     */
    public static boolean doReferencedMappingAlreadyExists(List<COREMapping<? extends EObject>> mappings,
            COREArtefact root) {
        Collection<COREMapping<? extends EObject>> firstMappings = EMFModelUtil.findCrossReferencesOfType(
                root.eContainer(), mappings.get(0),
                CorePackage.Literals.CORE_MAPPING__REFERENCED_MAPPINGS, CorePackage.Literals.CORE_MAPPING);
        
        for (int i = 1; i < mappings.size(); i++) {
            Collection<COREMapping<? extends EObject>> otherMappings = EMFModelUtil.findCrossReferencesOfType(
                    root.eContainer(), mappings.get(i),
                    CorePackage.Literals.CORE_MAPPING__REFERENCED_MAPPINGS, CorePackage.Literals.CORE_MAPPING);
            if (!Collections.disjoint(firstMappings, otherMappings)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Retrieve all the possible references mappings from a specific cardinality reference.
     * 
     * @param ciElement 
     * @param cardinalityReference 
     * @return a collection of COREMapping containing those possible references
     */
    public static Collection<COREMapping<? extends EObject>> getPossibleReferenceMappingsFromCardinalityRef(
            CORECIElement ciElement, COREMappingCardinality cardinalityReference) {
        COREExternalArtefact root = getReferencingExternalArtefact(ciElement.getModelElement());
        
        Collection<COREMapping<? extends EObject>> mappings = EMFModelUtil.findCrossReferencesOfType(
                root.eContainer(), gatherCIElementFromMappingCardinality(cardinalityReference, root).getModelElement(),
                CorePackage.Literals.CORE_LINK__FROM, CorePackage.Literals.CORE_MAPPING);

        return mappings;
    }
    
    /**
     * Gather the {@link CORECIElement} associated with the given {@link EObject} modelElement.
     * If no one exists, return null
     * @param modelElement - The model element associated with the CORECIElement
     * @return the CORECIElement associated
     */
    public static CORECIElement getCIElementFor(EObject modelElement) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(modelElement);
        
        if (artefact == null) {
            return null;
        }
        for (CORECIElement ciElement : artefact.getCiElements()) {
            if (modelElement == ciElement.getModelElement()) {
                return ciElement;
            }
        }
        return null;
    }
    
    /**
     * Gather the {@link CORECIElement} associated with the given {@link EObject} modelElement.
     * If no one exists, return null
     * @param modelElement - The model element associated with the CORECIElement
     * @param artefact - The artefact that goes with the model that contains the model element
     * @return the CORECIElement associated
     */
    public static CORECIElement getCIElementFor(EObject modelElement, COREArtefact artefact) {        
        if (artefact == null) {
            return null;
        }
        for (CORECIElement ciElement : artefact.getCiElements()) {
            if (modelElement == ciElement.getModelElement()) {
                return ciElement;
            }
        }
        return null;
    }
    
    /**
     * Gather the {@link COREUIElement} associated with the given {@link EObject} modelElement.
     * If no one exists, return null
     * @param modelElement - The model element associated with the CORECIElement
     * @param artefact - The artefact that goes with the model that contains the model element
     * @return the COREUIElement associated
     */
    public static COREUIElement getUIElementFor(EObject modelElement, COREArtefact artefact) {        
        if (artefact == null) {
            return null;
        }
        for (COREUIElement uiElement : artefact.getUiElements()) {
            if (modelElement == uiElement.getModelElement()) {
                return uiElement;
            }
        }
        return null;
    }
    
    /**
     * Gather the {@link COREUIElement} associated with the given {@link EObject} modelElement.
     * If no one exists, return null
     * @param modelElement - The model element associated with the CORECIElement
     * @return the COREUIElement associated
     */
    public static COREUIElement getUIElementFor(EObject modelElement) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(modelElement);
        
        if (artefact == null) {
            return null;
        }
        for (COREUIElement uiElement : artefact.getUiElements()) {
            if (modelElement == uiElement.getModelElement()) {
                return uiElement;
            }
        }
        return null;
    }
    
    /**
     * Operation that returns the partiality for a model element. If the element is not part of the
     * customization interface COREPartialityType.NONE is returned.
     * 
     * @param modelElement for which partiality is requested
     * @return the partiality of the model element
     */
    public static COREPartialityType getPartialityFor(EObject modelElement) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(modelElement);
        
        if (artefact == null) {
            return COREPartialityType.NONE;
        }
        for (CORECIElement ciElement : artefact.getCiElements()) {
            if (modelElement == ciElement.getModelElement()) {
                return ciElement.getPartiality();
            }
        }
        return null;        
    }
    
    /**
     * Gather all the elements along or mapped to the modelElement given.
     * Then retrieve the {@link CORECIElement} associated with those elements.
     * Return also the {@link CORECIElement} of the modelElement given.
     * 
     * @param modelElement - The element we want to gather the {@link CORECIElement} from
     * @return a set of {@link CORECIElement}
     */
    public static Set<CORECIElement> getAllCIElementAssociated(EObject modelElement) {
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(modelElement);
        Set<CORECIElement> result = new HashSet<>();
        
        // We gather all ciElement from this artefact
        for (CORECIElement ciElement : artefact.getCiElements()) {
            // Then we add all these elements and those mapped to them
            // No need to add the element in this ciElement because getAllMappedElementsRecursif returns it
            for (EObject elementMapped : getAllMappedElementsRecursif(ciElement.getModelElement())) {
                CORECIElement ciElementMapped = getCIElementFor(elementMapped);
                if (ciElementMapped != null) {
                    result.add(ciElementMapped);
                }
            }
        }
        
        return result;
    }
    
    /**
     * Depending on the {@link CORECIElement} associated with each element, filter it if it cannot be mapped.
     * 
     * @param elements - elements to filter
     * @param root - the elements root
     * @param mappingChanging - the mapping that already exists while selecting the mapping
     * @return elements filtered
     */
    public static Collection<EObject> filterPossibleMapping(Collection<? extends EObject> elements, 
            COREExternalArtefact root, COREMapping<? extends EObject> mappingChanging) {
        List<EObject> referenceMappingElements = new ArrayList<>();
        Map<COREMappingCardinality, EObject> existingMappingCardinality = new HashMap<>();
        List<EObject> result = new ArrayList<>();
        
        // find the artefact containaing the mapping
        EObject currentContainer = mappingChanging.eContainer();
        while (!(currentContainer instanceof COREArtefact)) {
            currentContainer = currentContainer.eContainer();
        }
        COREArtefact currentArtefact = (COREArtefact) currentContainer;
        
        // Filter the elements except for the ones that have a reference cardinality mapping
        for (EObject element : elements) {
            CORECIElement ciElement = getCIElementFor(element);
            if (ciElement != null && ciElement.getMappingCardinality() != null) {
                if (filterMappingCardinality(element, ciElement.getMappingCardinality(), root)) {
                    result.add(element);
                }
            } else if (ciElement == null && filterNoneCardinality(element, root, currentArtefact)) {
                result.add(element);
            } else if (ciElement != null) {
                referenceMappingElements.add(element);
            }
        }
        
        // Gather all elements that are already mapped from root and its extended / reuse artefacts
        Collection<COREExternalArtefact> artefacts = COREModelUtil.getExtendedAndReusedArtefacts(root);
        artefacts.add(root);
        for (COREExternalArtefact artefact : artefacts) {
            for (CORECIElement ciElement : artefact.getCiElements()) {
                if (ciElement.getMappingCardinality() != null 
                        && isElementMapped(ciElement.getModelElement(), artefact)) {
                    existingMappingCardinality.put(ciElement.getMappingCardinality(), ciElement.getModelElement());
                }
            }
        }
        
        
        // Filter the elements that have a reference cardinality mapping thanks to the existingMappingCardinality list
        for (EObject referenceMappingElement : referenceMappingElements) {
            // For now we don't allow a referenced mapping when we change an already existing mapping
            if (mappingChanging.getFrom() == null 
                    && filterReferenceCardinality(referenceMappingElement, existingMappingCardinality, root)) {
                result.add(referenceMappingElement);
            }
        }
        return result;
    }
    
    /**
     * Check if the element is already mapped.
     * 
     * @param element 
     * @param root - the element root
     * @return true if it can be mapped, false otherwise
     */
    public static boolean isElementMapped(EObject element, COREArtefact root) {
        Collection<COREMapping<? extends EObject>> mappings = EMFModelUtil.findCrossReferencesOfType(
                root.eContainer(), element,
                CorePackage.Literals.CORE_LINK__TO, CorePackage.Literals.CORE_MAPPING);
        return (mappings.size() >= 1) ? true : false;
    }

    /**
     * Check if the element is being mapped to.
     * 
     * @param element 
     * @param artefact - the external artefact that refers to the root of the model containing the element
     * @return true if the element is being mapped to
     */
    public static boolean isElementMappedTo(EObject element, COREArtefact artefact) {
        for (COREModelExtension cme : artefact.getModelExtensions()) {
            for (COREModelElementComposition<?> cc : cme.getCompositions()) {
                COREMapping<?> cm = (COREMapping<?>) cc;
                if (COREMappingUtil.isElementMappedTo(cm, element)) {
                    return true;
                }
            }
        }
        for (COREModelReuse cmr : artefact.getModelReuses()) {
            for (COREModelElementComposition<?> cc : cmr.getCompositions()) {
                COREMapping<?> cm = (COREMapping<?>) cc;
                if (COREMappingUtil.isElementMappedTo(cm, element)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    
    /**
     * Check if the mapping is already referenced by another mapping.
     * 
     * @param mapping 
     * @param root - the element root
     * @return true if the mapping is indeed referenced by another mapping
     */
    public static boolean isMappingAlreadyReferenced(COREMapping<? extends EObject> mapping, COREArtefact root) {
        Collection<COREMapping<? extends EObject>> mappings = EMFModelUtil.findCrossReferencesOfType(
                root.eContainer(), mapping,
                CorePackage.Literals.CORE_MAPPING__REFERENCED_MAPPINGS, CorePackage.Literals.CORE_MAPPING);
        return (mappings.size() >= 1) ? true : false;
    }
    
    /**
     * Check with different rules if the element that have references {@link COREMappingCardinality} can be mapped.
     * The references {@link COREMappingCardinality} must already be mapped
     * The references {@link COREMappingCardinality} multiplied between them must not surpass the mapping number of
     * this element
     * 
     * @param element 
     * @param mCardinalities - the references {@link COREMappingCardinality}
     * @param root - the element root
     * @return true if it can be mapped, false otherwise
     */
    private static boolean filterReferenceCardinality(EObject element, 
            Map<COREMappingCardinality, EObject> mCardinalities, COREArtefact root) {
        Collection<COREMapping<? extends EObject>> mappings = EMFModelUtil.findCrossReferencesOfType(
                root.eContainer(), element,
                CorePackage.Literals.CORE_LINK__FROM, CorePackage.Literals.CORE_MAPPING);

        Collection<COREMappingCardinality> referenceCardinalities = getCIElementFor(element).getReferenceCardinality();
        
        // First we check if we have all the reference cardinalities in mCardinalities
        if (!mCardinalities.keySet().containsAll(referenceCardinalities)) {
            return false;
        }
        
        int nbMappingPossible = 1;
        // The check to see that all the reference cardinalities are already mapped at least once is implicit
        // (if cardinalityMappings.size() = 0)
        for (COREMappingCardinality mCardinality : mCardinalities.keySet()) {
            if (referenceCardinalities.contains(mCardinality)) {
                Collection<COREMapping<? extends EObject>> cardinalityMappings = 
                        EMFModelUtil.findCrossReferencesOfType(root.eContainer(), mCardinalities.get(mCardinality),
                        CorePackage.Literals.CORE_LINK__FROM, CorePackage.Literals.CORE_MAPPING);
                nbMappingPossible *= cardinalityMappings.size();
            }
        }
        // Check that we can still have enough mapping available to make a new mapping for this reference
        if (mappings.size() >= nbMappingPossible) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Check with different rules if the element that have a cardinality {@link COREMappingCardinality} can be mapped.
     * The number of mapping must not surpass the {@link COREMappingCardinality} upperBound
     * 
     * @param element 
     * @param mCardinality - the cardinality {@link COREMappingCardinality}
     * @param root - the element root
     * @return true if it can be mapped, false otherwise
     */
    private static boolean filterMappingCardinality(EObject element, COREMappingCardinality mCardinality,
            COREArtefact root) {
        Collection<COREMapping<? extends EObject>> mappings = EMFModelUtil.findCrossReferencesOfType(
                root.eContainer(), element,
                CorePackage.Literals.CORE_LINK__FROM, CorePackage.Literals.CORE_MAPPING);
        if (mCardinality.getUpperBound() == -1 || mappings.size() < mCardinality.getUpperBound()) {
            return true;
        }
        return false;
    }
    
    /**
     * Check with different rules if the element that do not have a {@link CORECIElement} can be mapped.
     * The number of mapping must not surpass 1 for the current artefact
     * 
     * @param element 
     * @param root - the element root
     * @param currentArtefact - the artefact that contains the mapping
     * @return true if it can be mapped, false otherwise
     */
    private static boolean filterNoneCardinality(EObject element, COREArtefact root, COREArtefact currentArtefact) {
        Collection<COREMapping<? extends EObject>> mappings = EMFModelUtil.findCrossReferencesOfType(
                root.eContainer(), element,
                CorePackage.Literals.CORE_LINK__FROM, CorePackage.Literals.CORE_MAPPING);
        if (mappings.size() > 0) {
            // check whether there is more than one mapping where the "to" reference points to the same model
            int count = 0;
            for (COREMapping<? extends EObject> mapping : mappings) {
                if (mapping.eContainer().eContainer() == currentArtefact) {
                    count++;
                }
            }
            return count == 0;
        }
        return true;
    }
    
    /**
     * Return the text representation of a {@link CORECIELement}.
     * @param ciElement - The CORECIElement
     * @return the text representation of ciElement
     */
    public static String getMappingCardinalityText(CORECIElement ciElement) {
        StringBuilder res = new StringBuilder();
        COREMappingCardinality mappingCardinality = ciElement.getMappingCardinality();
        List<COREMappingCardinality> referenceCardinalities = ciElement.getReferenceCardinality();
        
        if (mappingCardinality != null) {
            if (mappingCardinality.getName() != null) {
                res.append(mappingCardinality.getName() + "=");
            }
            res.append(mappingCardinality.getLowerBound() + "..");
            if (mappingCardinality.getUpperBound() == -1) {
                res.append("*");
            } else {
                res.append(mappingCardinality.getUpperBound());
            }
        } else if (referenceCardinalities != null && referenceCardinalities.size() != 0) {
            // Otherwise, we add the reference cardinalities
            for (COREMappingCardinality referenceCardinality : referenceCardinalities) {
                res.append(referenceCardinality.getName() + "*");
            }
            res.deleteCharAt(res.length() - 1);
        }
        return res.toString();
    }
    
    /**
     * Create a set of combinations of all the lists given.
     * Example : [A, B] and [C, D] give [AB, AC, BC, BD]
     * 
     * @param lists 
     * @param <T> Not an actual parameter
     * @return a set of those lists combined
     */
    private static <T> Set<List<T>> getCombinations(List<List<T>> lists) {
        Set<List<T>> combinations = new HashSet<List<T>>();
        Set<List<T>> newCombinations;

        int index = 0;
        for (T i : lists.get(0)) {
            List<T> newList = new ArrayList<T>();
            newList.add(i);
            combinations.add(newList);
        }
        index++;
        while (index < lists.size()) {
            List<T> nextList = lists.get(index);
            newCombinations = new HashSet<List<T>>();
            for (List<T> first : combinations) {
                for (T second : nextList) {
                    List<T> newList = new ArrayList<T>();
                    newList.addAll(first);
                    newList.add(second);
                    newCombinations.add(newList);
                }
            }
            combinations = newCombinations;

            index++;
        }

        return combinations;
    }
    
    /** 
     * Operation that creates a copy of an external artefact as well as the external model
     * that is linked to it.
     * 
     * @param original the original external artefact (containing a model)
     * @return a copy of the external artefact, containing a copy of the model
     */
    public static COREExternalArtefact copyArtefactAndModel(COREExternalArtefact original) {

        // first copy the original model
        EObject model = original.getRootModelElement();
        HashMap<EObject, EObject> mappings = new HashMap<EObject, EObject>();
        EObject modelCopy = ModelCopier.copyModelAndRetrieveMappings(model, mappings);
        
        COREExternalArtefact artefactCopy = EcoreUtil.copy(original);
        
        // now update all mappings in the COREModelCompositions in the artefact copy to refer to the elements
        // in the model copy (instead of referring to the model elements in the original model)
        for (COREModelExtension me : artefactCopy.getModelExtensions()) {
            for (COREModelElementComposition<?> mec : me.getCompositions()) {
                if (mec instanceof CORELink<?>) {
                    @SuppressWarnings("unchecked")
                    CORELink<EObject> cl = (CORELink<EObject>) mec;
                    if (mappings.containsKey(cl.getTo())) {
                        cl.setTo(mappings.get(cl.getTo()));
                    }
                        
                }
            }
        }
        for (COREModelReuse me : artefactCopy.getModelReuses()) {
            for (COREModelElementComposition<?> mec : me.getCompositions()) {
                if (mec instanceof CORELink<?>) {
                    @SuppressWarnings("unchecked")
                    CORELink<EObject> cl = (CORELink<EObject>) mec;
                    if (mappings.containsKey(cl.getFrom())) {
                        cl.setTo(mappings.get(cl.getFrom()));
                    }
                        
                }
            }
        }
        
        // initialize the reuse reference of each model reuse copy to point to the reuse
        // in the concern
        int index = 0;
        EList<COREModelReuse> originalReuses = original.getModelReuses();
        EList<COREModelReuse> copiedReuses = artefactCopy.getModelReuses();
        while (index < originalReuses.size()) {
            copiedReuses.get(index).setReuse(originalReuses.get(index).getReuse());
            index++;
        }
       
        // associate the copied model with the copied artefact
        artefactCopy.setRootModelElement(modelCopy);
        
        return artefactCopy;
    }
    
    /**
     * Operation that collects all artefacts associated with a scene.
     * @param s the scene for which artefacts should be collected
     * @return a collection of artefacts
     */
    public static ArrayList<COREArtefact> getAllArtefactsOfScene(COREScene s) {
        ArrayList<COREArtefact> result = new ArrayList<COREArtefact>();
        
        Collection<EList<COREArtefact>> artefacts = s.getArtefacts().values();        
        for (EList<COREArtefact> arts : artefacts) {
            result.addAll(arts);            
        }
        
        return result;
    }
}
