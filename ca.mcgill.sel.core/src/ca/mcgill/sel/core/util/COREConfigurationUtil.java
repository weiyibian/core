package ca.mcgill.sel.core.util;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREFeature;

/**
 * This class provides helper methods to be handle {@link ca.mcgill.sel.core.COREConfiguration}s.
 *
 * @author CCamillieri
 */
public final class COREConfigurationUtil {

    /**
     * Prevent to instantiate.
     */
    private COREConfigurationUtil() {
    }

    /**
     * Get leaves features from a given configuration selection.
     * It looks at used {@link ca.mcgill.sel.core.COREConcernConfiguration} if any.
     * This method DOES not look into extended configurations, nor re-exposed features.
     *
     * The current implementation considers that the selection has no "hole",
     * ie considering the hierarchy of features A > B > C; if C is selected, A and B must be in the selected list
     * of the configuration as well.
     *
     * @param configuration - The configuration to check.
     * @return set of features that had no selected children.
     */
    public static Set<COREFeature> getSelectedLeaves(COREConfiguration configuration) {
        // Add selection from reused COREConcernConfiguaration
        Set<COREFeature> baseSelection = new HashSet<COREFeature>();
        baseSelection.addAll(configuration.getSelected());
        Set<COREFeature> leaves = new HashSet<COREFeature>();
        // For each feature from the selection, check if one of its child is selected
        loop: for (COREFeature feature : baseSelection) {
            for (COREFeature child : feature.getChildren()) {
                if (baseSelection.contains(child)) {
                    continue loop;
                }
            }
            // If we're here, we didn't find any selected descendant
            leaves.add(feature);
        }
        return leaves;
    }

    /**
     * Make a basic merge of two configurations. Does not merge extended configurations.
     *
     * @param base - The base configuration.
     * @param toMerge - The configuration to merge
     */
    public static void mergeConfigurations(COREConfiguration base, COREConfiguration toMerge) {
        mergeConfigurations(base, toMerge, false, false);
    }

    /**
     * Composes two configurations. Creates a new configuration to hold the result.
     *
     * @param base - The configuration to compose.
     * @param toMerge - The configuration to compose.
     * @return a new configuration into which left and right were merged.
     */
    public static COREConfiguration composeConfigurations(COREConfiguration base, COREConfiguration toMerge) {
        COREConfiguration result = EcoreUtil.copy(base);
        
        mergeConfigurations(result, toMerge, true, true);

        return result;
    }
    
    /**
     * Method that merges a hierarchy of extending configurations from right into left.
     * 
     * @param base the destination configuration
     * @param toMerge the one that is to be merged
     * @param mergeExtended - Whether to merge extended configurations as well or not.
     * @param addComplete - Whether to add extended configurations that are already complete or not
     */
    public static void mergeConfigurations(COREConfiguration base, COREConfiguration toMerge,
            boolean mergeExtended, boolean addComplete) {
        // merge selected features (union)
        for (COREFeature selected : toMerge.getSelected()) {
            if (!base.getSelected().contains(selected)) {
                base.getSelected().add(selected);
            }
        }
        // merge reExposed features (intersection)
        Set<COREFeature> deReExposed = new HashSet<COREFeature>();
        for (COREFeature reexposed : toMerge.getReexposed()) {
            if (!base.getReexposed().contains(reexposed)) {
                deReExposed.add(reexposed);
            }
        }
        base.getReexposed().removeAll(deReExposed);
        
        if (mergeExtended) {
            //merge the extending configurations, if any
            for (COREConfiguration rightExtending : toMerge.getExtendingConfigurations()) {
                for (COREConfiguration leftExtending : base.getExtendingConfigurations()) {
                    // check whether the configurations refer to the same reuse
                    if (rightExtending.getExtendedReuse() == leftExtending.getExtendedReuse()) {
                        mergeConfigurations(leftExtending, rightExtending);                    
                        // exit the inner loop and continue with the next rightExtending
                        break;
                    }
                    if (addComplete || !isConfigurationComplete(rightExtending)) {
                        // since we couldn't find a matching extending configuration in left, let's simply copy
                        // the extending configuration over
                        COREConfiguration rightCopy = EcoreUtil.copy(rightExtending);
                        base.getExtendingConfigurations().add(rightCopy);
                    }
                }
            }
        }
    }
    
    
    /**
     * Check if a configuration is complete.
     *
     * @param configuration - The configuration to check.
     * @return true if there is no reExposed feature left in the selection.
     */
    public static boolean isConfigurationComplete(COREConfiguration configuration) {
        if (configuration == null) {
            return false;
        }
        for (COREConfiguration extended : configuration.getExtendingConfigurations()) {
            if (!isConfigurationComplete(extended)) {
                return false;
            }
        }
        return configuration.getReexposed().isEmpty();
    }

}
