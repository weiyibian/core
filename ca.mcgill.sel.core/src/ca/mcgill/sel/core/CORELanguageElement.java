/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Language Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getLanguageElement <em>Language Element</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getCardinalities <em>Cardinalities</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.CORELanguageElement#getLanguage <em>Language</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement()
 * @model annotation="http://www.obeo.fr/dsl/dnc/archetype archetype='Role'"
 * @generated
 */
public interface CORELanguageElement extends EObject {

    /**
     * Returns the value of the '<em><b>Language Element</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Language Element</em>' reference.
     * @see #setLanguageElement(EClass)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_LanguageElement()
     * @model required="true"
     * @generated
     */
    EClass getLanguageElement();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElement#getLanguageElement <em>Language Element</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Language Element</em>' reference.
     * @see #getLanguageElement()
     * @generated
     */
    void setLanguageElement(EClass value);

    /**
     * Returns the value of the '<em><b>Cardinalities</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.ToCardinality}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.ToCardinality#getLanguageElement <em>Language Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Cardinalities</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_Cardinalities()
     * @see ca.mcgill.sel.core.ToCardinality#getLanguageElement
     * @model opposite="languageElement"
     * @generated
     */
    EList<ToCardinality> getCardinalities();

    /**
     * Returns the value of the '<em><b>Language</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.core.COREExternalLanguage#getLanguageElements <em>Language Elements</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Language</em>' container reference.
     * @see #setLanguage(COREExternalLanguage)
     * @see ca.mcgill.sel.core.CorePackage#getCORELanguageElement_Language()
     * @see ca.mcgill.sel.core.COREExternalLanguage#getLanguageElements
     * @model opposite="languageElements" required="true" transient="false"
     * @generated
     */
    COREExternalLanguage getLanguage();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORELanguageElement#getLanguage <em>Language</em>}' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Language</em>' container reference.
     * @see #getLanguage()
     * @generated
     */
    void setLanguage(COREExternalLanguage value);
} // CORELanguageElement
