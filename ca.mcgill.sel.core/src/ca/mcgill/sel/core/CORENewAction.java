/**
 */
package ca.mcgill.sel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE New Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORENewAction#getReusedActions <em>Reused Actions</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORENewAction()
 * @model
 * @generated
 */
public interface CORENewAction extends COREPerspectiveAction {
    /**
     * Returns the value of the '<em><b>Reused Actions</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.core.COREAction}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Reused Actions</em>' reference list.
     * @see ca.mcgill.sel.core.CorePackage#getCORENewAction_ReusedActions()
     * @model
     * @generated
     */
    EList<COREAction> getReusedActions();

} // CORENewAction
