/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORECreateMapping;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.CORERelationship;
import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Language Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getRelationship <em>Relationship</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getFromCardinality <em>From Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getInstances <em>Instances</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.CORELanguageElementMappingImpl#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class CORELanguageElementMappingImpl extends EObjectImpl implements CORELanguageElementMapping {
    /**
     * The default value of the '{@link #getRelationship() <em>Relationship</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getRelationship()
     * @generated
     * @ordered
     */
    protected static final CORERelationship RELATIONSHIP_EDEFAULT = CORERelationship.EQUALITY;

    /**
     * The cached value of the '{@link #getRelationship() <em>Relationship</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getRelationship()
     * @generated
     * @ordered
     */
    protected CORERelationship relationship = RELATIONSHIP_EDEFAULT;

    /**
     * The cached value of the '{@link #getActions() <em>Actions</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getActions()
     * @generated
     * @ordered
     */
    protected EList<CORECreateMapping> actions;

    /**
     * The default value of the '{@link #getFromCardinality() <em>From Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFromCardinality()
     * @generated
     * @ordered
     */
    protected static final Cardinality FROM_CARDINALITY_EDEFAULT = Cardinality.COMPULSORY;

    /**
     * The cached value of the '{@link #getFromCardinality() <em>From Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFromCardinality()
     * @generated
     * @ordered
     */
    protected Cardinality fromCardinality = FROM_CARDINALITY_EDEFAULT;

    /**
     * The cached value of the '{@link #getInstances() <em>Instances</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getInstances()
     * @generated
     * @ordered
     */
    protected EList<COREModelElementMapping> instances;

    /**
     * The cached value of the '{@link #getFrom() <em>From</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFrom()
     * @generated
     * @ordered
     */
    protected EObject from;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CORELanguageElementMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_LANGUAGE_ELEMENT_MAPPING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CORERelationship getRelationship() {
        return relationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setRelationship(CORERelationship newRelationship) {
        CORERelationship oldRelationship = relationship;
        relationship = newRelationship == null ? RELATIONSHIP_EDEFAULT : newRelationship;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__RELATIONSHIP, oldRelationship, relationship));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject getFrom() {
        if (from != null && from.eIsProxy()) {
            InternalEObject oldFrom = (InternalEObject)from;
            from = eResolveProxy(oldFrom);
            if (from != oldFrom) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM, oldFrom, from));
            }
        }
        return from;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EObject basicGetFrom() {
        return from;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setFrom(EObject newFrom) {
        EObject oldFrom = from;
        from = newFrom;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM, oldFrom, from));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<CORECreateMapping> getActions() {
        if (actions == null) {
            actions = new EObjectWithInverseResolvingEList<CORECreateMapping>(CORECreateMapping.class, this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS, CorePackage.CORE_CREATE_MAPPING__TYPE);
        }
        return actions;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Cardinality getFromCardinality() {
        return fromCardinality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setFromCardinality(Cardinality newFromCardinality) {
        Cardinality oldFromCardinality = fromCardinality;
        fromCardinality = newFromCardinality == null ? FROM_CARDINALITY_EDEFAULT : newFromCardinality;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM_CARDINALITY, oldFromCardinality, fromCardinality));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<COREModelElementMapping> getInstances() {
        if (instances == null) {
            instances = new EObjectWithInverseResolvingEList<COREModelElementMapping>(COREModelElementMapping.class, this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES, CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE);
        }
        return instances;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getActions()).basicAdd(otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getInstances()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES:
                return ((InternalEList<?>)getInstances()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__RELATIONSHIP:
                return getRelationship();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                return getActions();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM_CARDINALITY:
                return getFromCardinality();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES:
                return getInstances();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM:
                if (resolve) return getFrom();
                return basicGetFrom();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__RELATIONSHIP:
                setRelationship((CORERelationship)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                getActions().clear();
                getActions().addAll((Collection<? extends CORECreateMapping>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM_CARDINALITY:
                setFromCardinality((Cardinality)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES:
                getInstances().clear();
                getInstances().addAll((Collection<? extends COREModelElementMapping>)newValue);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM:
                setFrom((EObject)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__RELATIONSHIP:
                setRelationship(RELATIONSHIP_EDEFAULT);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                getActions().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM_CARDINALITY:
                setFromCardinality(FROM_CARDINALITY_EDEFAULT);
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES:
                getInstances().clear();
                return;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM:
                setFrom((EObject)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__RELATIONSHIP:
                return relationship != RELATIONSHIP_EDEFAULT;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS:
                return actions != null && !actions.isEmpty();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM_CARDINALITY:
                return fromCardinality != FROM_CARDINALITY_EDEFAULT;
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES:
                return instances != null && !instances.isEmpty();
            case CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__FROM:
                return from != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (relationship: ");
        result.append(relationship);
        result.append(", fromCardinality: ");
        result.append(fromCardinality);
        result.append(')');
        return result.toString();
    }

} //CORELanguageElementMappingImpl
