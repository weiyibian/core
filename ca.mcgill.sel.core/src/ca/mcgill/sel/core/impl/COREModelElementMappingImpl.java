/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Model Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelElementMappingImpl#getFrom <em>From</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelElementMappingImpl#getTo <em>To</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREModelElementMappingImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COREModelElementMappingImpl extends EObjectImpl implements COREModelElementMapping {
    /**
     * The cached value of the '{@link #getFrom() <em>From</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFrom()
     * @generated
     * @ordered
     */
    protected EObject from;

    /**
     * The cached value of the '{@link #getTo() <em>To</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTo()
     * @generated
     * @ordered
     */
    protected EObject to;

    /**
     * The cached value of the '{@link #getType() <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
    protected CORELanguageElementMapping type;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected COREModelElementMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MODEL_ELEMENT_MAPPING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject getFrom() {
        if (from != null && from.eIsProxy()) {
            InternalEObject oldFrom = (InternalEObject)from;
            from = eResolveProxy(oldFrom);
            if (from != oldFrom) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_MODEL_ELEMENT_MAPPING__FROM, oldFrom, from));
            }
        }
        return from;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EObject basicGetFrom() {
        return from;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setFrom(EObject newFrom) {
        EObject oldFrom = from;
        from = newFrom;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_ELEMENT_MAPPING__FROM, oldFrom, from));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject getTo() {
        if (to != null && to.eIsProxy()) {
            InternalEObject oldTo = (InternalEObject)to;
            to = eResolveProxy(oldTo);
            if (to != oldTo) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_MODEL_ELEMENT_MAPPING__TO, oldTo, to));
            }
        }
        return to;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EObject basicGetTo() {
        return to;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setTo(EObject newTo) {
        EObject oldTo = to;
        to = newTo;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_ELEMENT_MAPPING__TO, oldTo, to));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CORELanguageElementMapping getType() {
        if (type != null && type.eIsProxy()) {
            InternalEObject oldType = (InternalEObject)type;
            type = (CORELanguageElementMapping)eResolveProxy(oldType);
            if (type != oldType) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE, oldType, type));
            }
        }
        return type;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CORELanguageElementMapping basicGetType() {
        return type;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetType(CORELanguageElementMapping newType, NotificationChain msgs) {
        CORELanguageElementMapping oldType = type;
        type = newType;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE, oldType, newType);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setType(CORELanguageElementMapping newType) {
        if (newType != type) {
            NotificationChain msgs = null;
            if (type != null)
                msgs = ((InternalEObject)type).eInverseRemove(this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES, CORELanguageElementMapping.class, msgs);
            if (newType != null)
                msgs = ((InternalEObject)newType).eInverseAdd(this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES, CORELanguageElementMapping.class, msgs);
            msgs = basicSetType(newType, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE, newType, newType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE:
                if (type != null)
                    msgs = ((InternalEObject)type).eInverseRemove(this, CorePackage.CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES, CORELanguageElementMapping.class, msgs);
                return basicSetType((CORELanguageElementMapping)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE:
                return basicSetType(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__FROM:
                if (resolve) return getFrom();
                return basicGetFrom();
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TO:
                if (resolve) return getTo();
                return basicGetTo();
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE:
                if (resolve) return getType();
                return basicGetType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__FROM:
                setFrom((EObject)newValue);
                return;
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TO:
                setTo((EObject)newValue);
                return;
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE:
                setType((CORELanguageElementMapping)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__FROM:
                setFrom((EObject)null);
                return;
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TO:
                setTo((EObject)null);
                return;
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE:
                setType((CORELanguageElementMapping)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__FROM:
                return from != null;
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TO:
                return to != null;
            case CorePackage.CORE_MODEL_ELEMENT_MAPPING__TYPE:
                return type != null;
        }
        return super.eIsSet(featureID);
    }

} //COREModelElementMappingImpl
