/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.CORENewAction;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE New Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.CORENewActionImpl#getReusedActions <em>Reused Actions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CORENewActionImpl extends COREPerspectiveActionImpl implements CORENewAction {
    /**
     * The cached value of the '{@link #getReusedActions() <em>Reused Actions</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReusedActions()
     * @generated
     * @ordered
     */
    protected EList<COREAction> reusedActions;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CORENewActionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_NEW_ACTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<COREAction> getReusedActions() {
        if (reusedActions == null) {
            reusedActions = new EObjectResolvingEList<COREAction>(COREAction.class, this, CorePackage.CORE_NEW_ACTION__REUSED_ACTIONS);
        }
        return reusedActions;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_NEW_ACTION__REUSED_ACTIONS:
                return getReusedActions();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_NEW_ACTION__REUSED_ACTIONS:
                getReusedActions().clear();
                getReusedActions().addAll((Collection<? extends COREAction>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_NEW_ACTION__REUSED_ACTIONS:
                getReusedActions().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_NEW_ACTION__REUSED_ACTIONS:
                return reusedActions != null && !reusedActions.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //CORENewActionImpl
