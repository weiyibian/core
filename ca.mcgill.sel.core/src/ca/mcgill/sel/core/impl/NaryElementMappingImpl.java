/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.NaryElementMapping;
import ca.mcgill.sel.core.ToCardinality;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Nary Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.NaryElementMappingImpl#getCardinalities <em>Cardinalities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NaryElementMappingImpl extends CORELanguageElementMappingImpl implements NaryElementMapping {
    /**
     * The cached value of the '{@link #getCardinalities() <em>Cardinalities</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCardinalities()
     * @generated
     * @ordered
     */
    protected EList<ToCardinality> cardinalities;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected NaryElementMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.NARY_ELEMENT_MAPPING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<ToCardinality> getCardinalities() {
        if (cardinalities == null) {
            cardinalities = new EObjectWithInverseResolvingEList<ToCardinality>(ToCardinality.class, this, CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES, CorePackage.TO_CARDINALITY__ELEMENT_MAPPING);
        }
        return cardinalities;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getCardinalities()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES:
                return ((InternalEList<?>)getCardinalities()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES:
                return getCardinalities();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES:
                getCardinalities().clear();
                getCardinalities().addAll((Collection<? extends ToCardinality>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES:
                getCardinalities().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES:
                return cardinalities != null && !cardinalities.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //NaryElementMappingImpl
