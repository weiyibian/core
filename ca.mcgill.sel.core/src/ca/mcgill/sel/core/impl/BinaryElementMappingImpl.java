/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.BinaryElementMapping;
import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.BinaryElementMappingImpl#getToCardinality <em>To Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.BinaryElementMappingImpl#getTo <em>To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BinaryElementMappingImpl extends CORELanguageElementMappingImpl implements BinaryElementMapping {
    /**
     * The default value of the '{@link #getToCardinality() <em>To Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getToCardinality()
     * @generated
     * @ordered
     */
    protected static final Cardinality TO_CARDINALITY_EDEFAULT = Cardinality.COMPULSORY;

    /**
     * The cached value of the '{@link #getToCardinality() <em>To Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getToCardinality()
     * @generated
     * @ordered
     */
    protected Cardinality toCardinality = TO_CARDINALITY_EDEFAULT;

    /**
     * The cached value of the '{@link #getTo() <em>To</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTo()
     * @generated
     * @ordered
     */
    protected EObject to;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected BinaryElementMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.BINARY_ELEMENT_MAPPING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject getTo() {
        if (to != null && to.eIsProxy()) {
            InternalEObject oldTo = (InternalEObject)to;
            to = eResolveProxy(oldTo);
            if (to != oldTo) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.BINARY_ELEMENT_MAPPING__TO, oldTo, to));
            }
        }
        return to;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EObject basicGetTo() {
        return to;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setTo(EObject newTo) {
        EObject oldTo = to;
        to = newTo;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.BINARY_ELEMENT_MAPPING__TO, oldTo, to));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Cardinality getToCardinality() {
        return toCardinality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setToCardinality(Cardinality newToCardinality) {
        Cardinality oldToCardinality = toCardinality;
        toCardinality = newToCardinality == null ? TO_CARDINALITY_EDEFAULT : newToCardinality;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.BINARY_ELEMENT_MAPPING__TO_CARDINALITY, oldToCardinality, toCardinality));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.BINARY_ELEMENT_MAPPING__TO_CARDINALITY:
                return getToCardinality();
            case CorePackage.BINARY_ELEMENT_MAPPING__TO:
                if (resolve) return getTo();
                return basicGetTo();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.BINARY_ELEMENT_MAPPING__TO_CARDINALITY:
                setToCardinality((Cardinality)newValue);
                return;
            case CorePackage.BINARY_ELEMENT_MAPPING__TO:
                setTo((EObject)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.BINARY_ELEMENT_MAPPING__TO_CARDINALITY:
                setToCardinality(TO_CARDINALITY_EDEFAULT);
                return;
            case CorePackage.BINARY_ELEMENT_MAPPING__TO:
                setTo((EObject)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.BINARY_ELEMENT_MAPPING__TO_CARDINALITY:
                return toCardinality != TO_CARDINALITY_EDEFAULT;
            case CorePackage.BINARY_ELEMENT_MAPPING__TO:
                return to != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (toCardinality: ");
        result.append(toCardinality);
        result.append(')');
        return result.toString();
    }

} //BinaryElementMappingImpl
