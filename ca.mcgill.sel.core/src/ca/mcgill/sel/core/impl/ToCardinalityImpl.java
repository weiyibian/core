/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.NaryElementMapping;
import ca.mcgill.sel.core.ToCardinality;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>To Cardinality</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.ToCardinalityImpl#getToCardinality <em>To Cardinality</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.ToCardinalityImpl#getElementMapping <em>Element Mapping</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.ToCardinalityImpl#getLanguageElement <em>Language Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ToCardinalityImpl extends EObjectImpl implements ToCardinality {
    /**
     * The default value of the '{@link #getToCardinality() <em>To Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getToCardinality()
     * @generated
     * @ordered
     */
    protected static final Cardinality TO_CARDINALITY_EDEFAULT = Cardinality.COMPULSORY;

    /**
     * The cached value of the '{@link #getToCardinality() <em>To Cardinality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getToCardinality()
     * @generated
     * @ordered
     */
    protected Cardinality toCardinality = TO_CARDINALITY_EDEFAULT;

    /**
     * The cached value of the '{@link #getElementMapping() <em>Element Mapping</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getElementMapping()
     * @generated
     * @ordered
     */
    protected NaryElementMapping elementMapping;

    /**
     * The cached value of the '{@link #getLanguageElement() <em>Language Element</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getLanguageElement()
     * @generated
     * @ordered
     */
    protected EObject languageElement;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ToCardinalityImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.TO_CARDINALITY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Cardinality getToCardinality() {
        return toCardinality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setToCardinality(Cardinality newToCardinality) {
        Cardinality oldToCardinality = toCardinality;
        toCardinality = newToCardinality == null ? TO_CARDINALITY_EDEFAULT : newToCardinality;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.TO_CARDINALITY__TO_CARDINALITY, oldToCardinality, toCardinality));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NaryElementMapping getElementMapping() {
        if (elementMapping != null && elementMapping.eIsProxy()) {
            InternalEObject oldElementMapping = (InternalEObject)elementMapping;
            elementMapping = (NaryElementMapping)eResolveProxy(oldElementMapping);
            if (elementMapping != oldElementMapping) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.TO_CARDINALITY__ELEMENT_MAPPING, oldElementMapping, elementMapping));
            }
        }
        return elementMapping;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NaryElementMapping basicGetElementMapping() {
        return elementMapping;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetElementMapping(NaryElementMapping newElementMapping, NotificationChain msgs) {
        NaryElementMapping oldElementMapping = elementMapping;
        elementMapping = newElementMapping;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.TO_CARDINALITY__ELEMENT_MAPPING, oldElementMapping, newElementMapping);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setElementMapping(NaryElementMapping newElementMapping) {
        if (newElementMapping != elementMapping) {
            NotificationChain msgs = null;
            if (elementMapping != null)
                msgs = ((InternalEObject)elementMapping).eInverseRemove(this, CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES, NaryElementMapping.class, msgs);
            if (newElementMapping != null)
                msgs = ((InternalEObject)newElementMapping).eInverseAdd(this, CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES, NaryElementMapping.class, msgs);
            msgs = basicSetElementMapping(newElementMapping, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.TO_CARDINALITY__ELEMENT_MAPPING, newElementMapping, newElementMapping));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject getLanguageElement() {
        if (languageElement != null && languageElement.eIsProxy()) {
            InternalEObject oldLanguageElement = (InternalEObject)languageElement;
            languageElement = eResolveProxy(oldLanguageElement);
            if (languageElement != oldLanguageElement) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.TO_CARDINALITY__LANGUAGE_ELEMENT, oldLanguageElement, languageElement));
            }
        }
        return languageElement;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EObject basicGetLanguageElement() {
        return languageElement;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setLanguageElement(EObject newLanguageElement) {
        EObject oldLanguageElement = languageElement;
        languageElement = newLanguageElement;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.TO_CARDINALITY__LANGUAGE_ELEMENT, oldLanguageElement, languageElement));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.TO_CARDINALITY__ELEMENT_MAPPING:
                if (elementMapping != null)
                    msgs = ((InternalEObject)elementMapping).eInverseRemove(this, CorePackage.NARY_ELEMENT_MAPPING__CARDINALITIES, NaryElementMapping.class, msgs);
                return basicSetElementMapping((NaryElementMapping)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.TO_CARDINALITY__ELEMENT_MAPPING:
                return basicSetElementMapping(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.TO_CARDINALITY__TO_CARDINALITY:
                return getToCardinality();
            case CorePackage.TO_CARDINALITY__ELEMENT_MAPPING:
                if (resolve) return getElementMapping();
                return basicGetElementMapping();
            case CorePackage.TO_CARDINALITY__LANGUAGE_ELEMENT:
                if (resolve) return getLanguageElement();
                return basicGetLanguageElement();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.TO_CARDINALITY__TO_CARDINALITY:
                setToCardinality((Cardinality)newValue);
                return;
            case CorePackage.TO_CARDINALITY__ELEMENT_MAPPING:
                setElementMapping((NaryElementMapping)newValue);
                return;
            case CorePackage.TO_CARDINALITY__LANGUAGE_ELEMENT:
                setLanguageElement((EObject)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.TO_CARDINALITY__TO_CARDINALITY:
                setToCardinality(TO_CARDINALITY_EDEFAULT);
                return;
            case CorePackage.TO_CARDINALITY__ELEMENT_MAPPING:
                setElementMapping((NaryElementMapping)null);
                return;
            case CorePackage.TO_CARDINALITY__LANGUAGE_ELEMENT:
                setLanguageElement((EObject)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.TO_CARDINALITY__TO_CARDINALITY:
                return toCardinality != TO_CARDINALITY_EDEFAULT;
            case CorePackage.TO_CARDINALITY__ELEMENT_MAPPING:
                return elementMapping != null;
            case CorePackage.TO_CARDINALITY__LANGUAGE_ELEMENT:
                return languageElement != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (toCardinality: ");
        result.append(toCardinality);
        result.append(')');
        return result.toString();
    }

} //ToCardinalityImpl
