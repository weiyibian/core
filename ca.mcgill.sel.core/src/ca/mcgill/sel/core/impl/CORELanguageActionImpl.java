/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CorePackage;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Language Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CORELanguageActionImpl extends COREActionImpl implements CORELanguageAction {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CORELanguageActionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_LANGUAGE_ACTION;
    }

} //CORELanguageActionImpl
