/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREPerspectiveAction;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Perspective Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class COREPerspectiveActionImpl extends COREActionImpl implements COREPerspectiveAction {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected COREPerspectiveActionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_PERSPECTIVE_ACTION;
    }

} //COREPerspectiveActionImpl
