/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.BinaryElementMapping;
import ca.mcgill.sel.core.COREAction;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.CORECreateMapping;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.CORELanguageAction;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.CORENewAction;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREPerspectiveAction;
import ca.mcgill.sel.core.CORERedefineAction;
import ca.mcgill.sel.core.COREReexposeAction;
import ca.mcgill.sel.core.CORERelationship;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.COREUIElement;
import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.LayoutElement;
import ca.mcgill.sel.core.NaryElementMapping;
import ca.mcgill.sel.core.ToCardinality;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CorePackageImpl extends EPackageImpl implements CorePackage {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreArtefactEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreImpactModelEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreConcernEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreFeatureEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreModelCompositionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreLinkEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreMappingEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreSceneEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass corePerspectiveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreExternalLanguageEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreLanguageEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreNewActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreModelElementMappingEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass toCardinalityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass languageMapEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass artefactMapEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass corePerspectiveActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreExternalArtefactEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreuiElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreciElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreMappingCardinalityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreLanguageElementMappingEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreReexposeActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreRedefineActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreLanguageActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreCreateMappingEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass binaryElementMappingEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass naryElementMappingEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreNamedElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreReuseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreImpactNodeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreConfigurationEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreFeatureModelEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreModelReuseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreContributionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass layoutMapEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass layoutElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass layoutContainerMapEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreFeatureImpactNodeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreModelElementCompositionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreWeightedLinkEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass coreModelExtensionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum coreFeatureRelationshipTypeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum coreVisibilityTypeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum corePartialityTypeEEnum = null;

    /**
     * @generated
     */
    private EEnum coreRelationshipEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum cardinalityEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see ca.mcgill.sel.core.CorePackage#eNS_URI
     * @see #init()
     * @generated
     */
    private CorePackageImpl() {
        super(eNS_URI, CoreFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link CorePackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static CorePackage init() {
        if (isInited) return (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

        // Obtain or create and register package
        Object registeredCorePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        CorePackageImpl theCorePackage = registeredCorePackage instanceof CorePackageImpl ? (CorePackageImpl)registeredCorePackage : new CorePackageImpl();

        isInited = true;

        // Create package meta-data objects
        theCorePackage.createPackageContents();

        // Initialize created meta-data
        theCorePackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theCorePackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(CorePackage.eNS_URI, theCorePackage);
        return theCorePackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREArtefact() {
        return coreArtefactEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREArtefact_ModelReuses() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREArtefact_CoreConcern() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREArtefact_ModelExtensions() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREArtefact_UiElements() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREArtefact_CiElements() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREArtefact_TemporaryConcern() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(5);
    }

                /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREArtefact_Scene() {
        return (EReference)coreArtefactEClass.getEStructuralFeatures().get(6);
    }

                /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREImpactModel() {
        return coreImpactModelEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREImpactModel_ImpactModelElements() {
        return (EReference)coreImpactModelEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREImpactModel_Layouts() {
        return (EReference)coreImpactModelEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREImpactModel_Contributions() {
        return (EReference)coreImpactModelEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREConcern() {
        return coreConcernEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConcern_Artefacts() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConcern_FeatureModel() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConcern_ImpactModel() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConcern_Scenes() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConcern_Reuses() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConcern_TemporaryArtefacts() {
        return (EReference)coreConcernEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREFeature() {
        return coreFeatureEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeature_RealizedBy() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeature_Children() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeature_Parent() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREFeature_ParentRelationship() {
        return (EAttribute)coreFeatureEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeature_Requires() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeature_Excludes() {
        return (EReference)coreFeatureEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREModelComposition() {
        return coreModelCompositionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREModelComposition_Source() {
        return (EReference)coreModelCompositionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREModelComposition_Compositions() {
        return (EReference)coreModelCompositionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORELink() {
        return coreLinkEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORELink_To() {
        return (EReference)coreLinkEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORELink_From() {
        return (EReference)coreLinkEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREMapping() {
        return coreMappingEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREMapping_Mappings() {
        return (EReference)coreMappingEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREMapping_ReferencedMappings() {
        return (EReference)coreMappingEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREScene() {
        return coreSceneEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREScene_Realizes() {
        return (EReference)coreSceneEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREScene_ElementMappings() {
        return (EReference)coreSceneEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREScene_Artefacts() {
        return (EReference)coreSceneEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREScene_PerspectiveName() {
        return (EAttribute)coreSceneEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREPerspective() {
        return corePerspectiveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREPerspective_Mappings() {
        return (EReference)corePerspectiveEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREPerspective_Languages() {
        return (EReference)corePerspectiveEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREPerspective_Default() {
        return (EAttribute)corePerspectiveEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREExternalLanguage() {
        return coreExternalLanguageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREExternalLanguage_NsURI() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREExternalLanguage_ResourceFactory() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREExternalLanguage_AdapterFactory() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREExternalLanguage_WeaverClassName() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREExternalLanguage_FileExtension() {
        return (EAttribute)coreExternalLanguageEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORELanguage() {
        return coreLanguageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORELanguage_Actions() {
        return (EReference)coreLanguageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREAction() {
        return coreActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREAction_Name() {
        return (EAttribute)coreActionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORENewAction() {
        return coreNewActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORENewAction_ReusedActions() {
        return (EReference)coreNewActionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREModelElementMapping() {
        return coreModelElementMappingEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREModelElementMapping_From() {
        return (EReference)coreModelElementMappingEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREModelElementMapping_To() {
        return (EReference)coreModelElementMappingEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREModelElementMapping_Type() {
        return (EReference)coreModelElementMappingEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getToCardinality() {
        return toCardinalityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getToCardinality_ToCardinality() {
        return (EAttribute)toCardinalityEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getToCardinality_ElementMapping() {
        return (EReference)toCardinalityEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getToCardinality_LanguageElement() {
        return (EReference)toCardinalityEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLanguageMap() {
        return languageMapEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLanguageMap_Key() {
        return (EAttribute)languageMapEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLanguageMap_Value() {
        return (EReference)languageMapEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getArtefactMap() {
        return artefactMapEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getArtefactMap_Value() {
        return (EReference)artefactMapEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getArtefactMap_Key() {
        return (EAttribute)artefactMapEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREPerspectiveAction() {
        return corePerspectiveActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREExternalArtefact() {
        return coreExternalArtefactEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREExternalArtefact_RootModelElement() {
        return (EReference)coreExternalArtefactEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREExternalArtefact_LanguageName() {
        return (EAttribute)coreExternalArtefactEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREUIElement() {
        return coreuiElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREUIElement_ModelElement() {
        return (EReference)coreuiElementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORECIElement() {
        return coreciElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCORECIElement_Partiality() {
        return (EAttribute)coreciElementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORECIElement_ModelElement() {
        return (EReference)coreciElementEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORECIElement_MappingCardinality() {
        return (EReference)coreciElementEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORECIElement_ReferenceCardinality() {
        return (EReference)coreciElementEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREMappingCardinality() {
        return coreMappingCardinalityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREMappingCardinality_LowerBound() {
        return (EAttribute)coreMappingCardinalityEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREMappingCardinality_UpperBound() {
        return (EAttribute)coreMappingCardinalityEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREMappingCardinality_Name() {
        return (EAttribute)coreMappingCardinalityEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORELanguageElementMapping() {
        return coreLanguageElementMappingEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCORELanguageElementMapping_Relationship() {
        return (EAttribute)coreLanguageElementMappingEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORELanguageElementMapping_Actions() {
        return (EReference)coreLanguageElementMappingEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCORELanguageElementMapping_FromCardinality() {
        return (EAttribute)coreLanguageElementMappingEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORELanguageElementMapping_Instances() {
        return (EReference)coreLanguageElementMappingEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORELanguageElementMapping_From() {
        return (EReference)coreLanguageElementMappingEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREReexposeAction() {
        return coreReexposeActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREReexposeAction_ReexposedAction() {
        return (EReference)coreReexposeActionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORERedefineAction() {
        return coreRedefineActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORERedefineAction_RedefinedAction() {
        return (EReference)coreRedefineActionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORERedefineAction_ReusedActions() {
        return (EReference)coreRedefineActionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORELanguageAction() {
        return coreLanguageActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORECreateMapping() {
        return coreCreateMappingEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORECreateMapping_Type() {
        return (EReference)coreCreateMappingEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCORECreateMapping_ExtendedAction() {
        return (EReference)coreCreateMappingEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBinaryElementMapping() {
        return binaryElementMappingEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBinaryElementMapping_ToCardinality() {
        return (EAttribute)binaryElementMappingEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBinaryElementMapping_To() {
        return (EReference)binaryElementMappingEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNaryElementMapping() {
        return naryElementMappingEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNaryElementMapping_Cardinalities() {
        return (EReference)naryElementMappingEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCORENamedElement() {
        return coreNamedElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCORENamedElement_Name() {
        return (EAttribute)coreNamedElementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREReuse() {
        return coreReuseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREReuse_ReusedConcern() {
        return (EReference)coreReuseEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREReuse_Extends() {
        return (EReference)coreReuseEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREReuse_ModelReuses() {
        return (EReference)coreReuseEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREImpactNode() {
        return coreImpactNodeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREImpactNode_ScalingFactor() {
        return (EAttribute)coreImpactNodeEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREImpactNode_Offset() {
        return (EAttribute)coreImpactNodeEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREImpactNode_Outgoing() {
        return (EReference)coreImpactNodeEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREImpactNode_Incoming() {
        return (EReference)coreImpactNodeEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREConfiguration() {
        return coreConfigurationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConfiguration_Selected() {
        return (EReference)coreConfigurationEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConfiguration_Reexposed() {
        return (EReference)coreConfigurationEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConfiguration_ExtendingConfigurations() {
        return (EReference)coreConfigurationEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREConfiguration_ExtendedReuse() {
        return (EReference)coreConfigurationEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREFeatureModel() {
        return coreFeatureModelEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeatureModel_Features() {
        return (EReference)coreFeatureModelEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeatureModel_Root() {
        return (EReference)coreFeatureModelEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREModelReuse() {
        return coreModelReuseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREModelReuse_Reuse() {
        return (EReference)coreModelReuseEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREModelReuse_Configuration() {
        return (EReference)coreModelReuseEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREContribution() {
        return coreContributionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREContribution_RelativeWeight() {
        return (EAttribute)coreContributionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREContribution_Source() {
        return (EReference)coreContributionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREContribution_Impacts() {
        return (EReference)coreContributionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLayoutMap() {
        return layoutMapEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLayoutMap_Key() {
        return (EReference)layoutMapEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLayoutMap_Value() {
        return (EReference)layoutMapEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLayoutElement() {
        return layoutElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLayoutElement_X() {
        return (EAttribute)layoutElementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLayoutElement_Y() {
        return (EAttribute)layoutElementEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLayoutContainerMap() {
        return layoutContainerMapEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLayoutContainerMap_Key() {
        return (EReference)layoutContainerMapEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLayoutContainerMap_Value() {
        return (EReference)layoutContainerMapEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREFeatureImpactNode() {
        return coreFeatureImpactNodeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREFeatureImpactNode_RelativeFeatureWeight() {
        return (EAttribute)coreFeatureImpactNodeEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeatureImpactNode_Represents() {
        return (EReference)coreFeatureImpactNodeEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCOREFeatureImpactNode_WeightedLinks() {
        return (EReference)coreFeatureImpactNodeEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREModelElementComposition() {
        return coreModelElementCompositionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREWeightedLink() {
        return coreWeightedLinkEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCOREWeightedLink_Weight() {
        return (EAttribute)coreWeightedLinkEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCOREModelExtension() {
        return coreModelExtensionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCOREFeatureRelationshipType() {
        return coreFeatureRelationshipTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCOREVisibilityType() {
        return coreVisibilityTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCOREPartialityType() {
        return corePartialityTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCORERelationship() {
        return coreRelationshipEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCardinality() {
        return cardinalityEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CoreFactory getCoreFactory() {
        return (CoreFactory)getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        coreArtefactEClass = createEClass(CORE_ARTEFACT);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__MODEL_REUSES);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__CORE_CONCERN);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__MODEL_EXTENSIONS);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__UI_ELEMENTS);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__CI_ELEMENTS);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__TEMPORARY_CONCERN);
        createEReference(coreArtefactEClass, CORE_ARTEFACT__SCENE);

        coreImpactModelEClass = createEClass(CORE_IMPACT_MODEL);
        createEReference(coreImpactModelEClass, CORE_IMPACT_MODEL__IMPACT_MODEL_ELEMENTS);
        createEReference(coreImpactModelEClass, CORE_IMPACT_MODEL__LAYOUTS);
        createEReference(coreImpactModelEClass, CORE_IMPACT_MODEL__CONTRIBUTIONS);

        coreConcernEClass = createEClass(CORE_CONCERN);
        createEReference(coreConcernEClass, CORE_CONCERN__ARTEFACTS);
        createEReference(coreConcernEClass, CORE_CONCERN__FEATURE_MODEL);
        createEReference(coreConcernEClass, CORE_CONCERN__IMPACT_MODEL);
        createEReference(coreConcernEClass, CORE_CONCERN__SCENES);
        createEReference(coreConcernEClass, CORE_CONCERN__REUSES);
        createEReference(coreConcernEClass, CORE_CONCERN__TEMPORARY_ARTEFACTS);

        coreFeatureEClass = createEClass(CORE_FEATURE);
        createEReference(coreFeatureEClass, CORE_FEATURE__REALIZED_BY);
        createEReference(coreFeatureEClass, CORE_FEATURE__CHILDREN);
        createEReference(coreFeatureEClass, CORE_FEATURE__PARENT);
        createEAttribute(coreFeatureEClass, CORE_FEATURE__PARENT_RELATIONSHIP);
        createEReference(coreFeatureEClass, CORE_FEATURE__REQUIRES);
        createEReference(coreFeatureEClass, CORE_FEATURE__EXCLUDES);

        coreModelCompositionEClass = createEClass(CORE_MODEL_COMPOSITION);
        createEReference(coreModelCompositionEClass, CORE_MODEL_COMPOSITION__SOURCE);
        createEReference(coreModelCompositionEClass, CORE_MODEL_COMPOSITION__COMPOSITIONS);

        coreLinkEClass = createEClass(CORE_LINK);
        createEReference(coreLinkEClass, CORE_LINK__TO);
        createEReference(coreLinkEClass, CORE_LINK__FROM);

        coreNamedElementEClass = createEClass(CORE_NAMED_ELEMENT);
        createEAttribute(coreNamedElementEClass, CORE_NAMED_ELEMENT__NAME);

        coreReuseEClass = createEClass(CORE_REUSE);
        createEReference(coreReuseEClass, CORE_REUSE__REUSED_CONCERN);
        createEReference(coreReuseEClass, CORE_REUSE__EXTENDS);
        createEReference(coreReuseEClass, CORE_REUSE__MODEL_REUSES);

        coreImpactNodeEClass = createEClass(CORE_IMPACT_NODE);
        createEAttribute(coreImpactNodeEClass, CORE_IMPACT_NODE__SCALING_FACTOR);
        createEAttribute(coreImpactNodeEClass, CORE_IMPACT_NODE__OFFSET);
        createEReference(coreImpactNodeEClass, CORE_IMPACT_NODE__OUTGOING);
        createEReference(coreImpactNodeEClass, CORE_IMPACT_NODE__INCOMING);

        coreConfigurationEClass = createEClass(CORE_CONFIGURATION);
        createEReference(coreConfigurationEClass, CORE_CONFIGURATION__SELECTED);
        createEReference(coreConfigurationEClass, CORE_CONFIGURATION__REEXPOSED);
        createEReference(coreConfigurationEClass, CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS);
        createEReference(coreConfigurationEClass, CORE_CONFIGURATION__EXTENDED_REUSE);

        coreFeatureModelEClass = createEClass(CORE_FEATURE_MODEL);
        createEReference(coreFeatureModelEClass, CORE_FEATURE_MODEL__FEATURES);
        createEReference(coreFeatureModelEClass, CORE_FEATURE_MODEL__ROOT);

        coreModelReuseEClass = createEClass(CORE_MODEL_REUSE);
        createEReference(coreModelReuseEClass, CORE_MODEL_REUSE__REUSE);
        createEReference(coreModelReuseEClass, CORE_MODEL_REUSE__CONFIGURATION);

        coreContributionEClass = createEClass(CORE_CONTRIBUTION);
        createEAttribute(coreContributionEClass, CORE_CONTRIBUTION__RELATIVE_WEIGHT);
        createEReference(coreContributionEClass, CORE_CONTRIBUTION__SOURCE);
        createEReference(coreContributionEClass, CORE_CONTRIBUTION__IMPACTS);

        layoutMapEClass = createEClass(LAYOUT_MAP);
        createEReference(layoutMapEClass, LAYOUT_MAP__KEY);
        createEReference(layoutMapEClass, LAYOUT_MAP__VALUE);

        layoutElementEClass = createEClass(LAYOUT_ELEMENT);
        createEAttribute(layoutElementEClass, LAYOUT_ELEMENT__X);
        createEAttribute(layoutElementEClass, LAYOUT_ELEMENT__Y);

        layoutContainerMapEClass = createEClass(LAYOUT_CONTAINER_MAP);
        createEReference(layoutContainerMapEClass, LAYOUT_CONTAINER_MAP__KEY);
        createEReference(layoutContainerMapEClass, LAYOUT_CONTAINER_MAP__VALUE);

        coreFeatureImpactNodeEClass = createEClass(CORE_FEATURE_IMPACT_NODE);
        createEAttribute(coreFeatureImpactNodeEClass, CORE_FEATURE_IMPACT_NODE__RELATIVE_FEATURE_WEIGHT);
        createEReference(coreFeatureImpactNodeEClass, CORE_FEATURE_IMPACT_NODE__REPRESENTS);
        createEReference(coreFeatureImpactNodeEClass, CORE_FEATURE_IMPACT_NODE__WEIGHTED_LINKS);

        coreModelElementCompositionEClass = createEClass(CORE_MODEL_ELEMENT_COMPOSITION);

        coreWeightedLinkEClass = createEClass(CORE_WEIGHTED_LINK);
        createEAttribute(coreWeightedLinkEClass, CORE_WEIGHTED_LINK__WEIGHT);

        coreModelExtensionEClass = createEClass(CORE_MODEL_EXTENSION);

        coreMappingEClass = createEClass(CORE_MAPPING);
        createEReference(coreMappingEClass, CORE_MAPPING__MAPPINGS);
        createEReference(coreMappingEClass, CORE_MAPPING__REFERENCED_MAPPINGS);

        coreSceneEClass = createEClass(CORE_SCENE);
        createEReference(coreSceneEClass, CORE_SCENE__REALIZES);
        createEAttribute(coreSceneEClass, CORE_SCENE__PERSPECTIVE_NAME);
        createEReference(coreSceneEClass, CORE_SCENE__ELEMENT_MAPPINGS);
        createEReference(coreSceneEClass, CORE_SCENE__ARTEFACTS);

        corePerspectiveEClass = createEClass(CORE_PERSPECTIVE);
        createEReference(corePerspectiveEClass, CORE_PERSPECTIVE__MAPPINGS);
        createEReference(corePerspectiveEClass, CORE_PERSPECTIVE__LANGUAGES);
        createEAttribute(corePerspectiveEClass, CORE_PERSPECTIVE__DEFAULT);

        coreExternalLanguageEClass = createEClass(CORE_EXTERNAL_LANGUAGE);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__NS_URI);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__RESOURCE_FACTORY);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__ADAPTER_FACTORY);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__WEAVER_CLASS_NAME);
        createEAttribute(coreExternalLanguageEClass, CORE_EXTERNAL_LANGUAGE__FILE_EXTENSION);

        corePerspectiveActionEClass = createEClass(CORE_PERSPECTIVE_ACTION);

        coreExternalArtefactEClass = createEClass(CORE_EXTERNAL_ARTEFACT);
        createEReference(coreExternalArtefactEClass, CORE_EXTERNAL_ARTEFACT__ROOT_MODEL_ELEMENT);
        createEAttribute(coreExternalArtefactEClass, CORE_EXTERNAL_ARTEFACT__LANGUAGE_NAME);

        coreuiElementEClass = createEClass(COREUI_ELEMENT);
        createEReference(coreuiElementEClass, COREUI_ELEMENT__MODEL_ELEMENT);

        coreciElementEClass = createEClass(CORECI_ELEMENT);
        createEAttribute(coreciElementEClass, CORECI_ELEMENT__PARTIALITY);
        createEReference(coreciElementEClass, CORECI_ELEMENT__MODEL_ELEMENT);
        createEReference(coreciElementEClass, CORECI_ELEMENT__MAPPING_CARDINALITY);
        createEReference(coreciElementEClass, CORECI_ELEMENT__REFERENCE_CARDINALITY);

        coreMappingCardinalityEClass = createEClass(CORE_MAPPING_CARDINALITY);
        createEAttribute(coreMappingCardinalityEClass, CORE_MAPPING_CARDINALITY__LOWER_BOUND);
        createEAttribute(coreMappingCardinalityEClass, CORE_MAPPING_CARDINALITY__UPPER_BOUND);
        createEAttribute(coreMappingCardinalityEClass, CORE_MAPPING_CARDINALITY__NAME);

        coreLanguageElementMappingEClass = createEClass(CORE_LANGUAGE_ELEMENT_MAPPING);
        createEAttribute(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__RELATIONSHIP);
        createEReference(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__ACTIONS);
        createEAttribute(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__FROM_CARDINALITY);
        createEReference(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__INSTANCES);
        createEReference(coreLanguageElementMappingEClass, CORE_LANGUAGE_ELEMENT_MAPPING__FROM);

        coreReexposeActionEClass = createEClass(CORE_REEXPOSE_ACTION);
        createEReference(coreReexposeActionEClass, CORE_REEXPOSE_ACTION__REEXPOSED_ACTION);

        coreRedefineActionEClass = createEClass(CORE_REDEFINE_ACTION);
        createEReference(coreRedefineActionEClass, CORE_REDEFINE_ACTION__REDEFINED_ACTION);
        createEReference(coreRedefineActionEClass, CORE_REDEFINE_ACTION__REUSED_ACTIONS);

        coreLanguageActionEClass = createEClass(CORE_LANGUAGE_ACTION);

        coreCreateMappingEClass = createEClass(CORE_CREATE_MAPPING);
        createEReference(coreCreateMappingEClass, CORE_CREATE_MAPPING__TYPE);
        createEReference(coreCreateMappingEClass, CORE_CREATE_MAPPING__EXTENDED_ACTION);

        binaryElementMappingEClass = createEClass(BINARY_ELEMENT_MAPPING);
        createEAttribute(binaryElementMappingEClass, BINARY_ELEMENT_MAPPING__TO_CARDINALITY);
        createEReference(binaryElementMappingEClass, BINARY_ELEMENT_MAPPING__TO);

        naryElementMappingEClass = createEClass(NARY_ELEMENT_MAPPING);
        createEReference(naryElementMappingEClass, NARY_ELEMENT_MAPPING__CARDINALITIES);

        coreLanguageEClass = createEClass(CORE_LANGUAGE);
        createEReference(coreLanguageEClass, CORE_LANGUAGE__ACTIONS);

        coreActionEClass = createEClass(CORE_ACTION);
        createEAttribute(coreActionEClass, CORE_ACTION__NAME);

        coreNewActionEClass = createEClass(CORE_NEW_ACTION);
        createEReference(coreNewActionEClass, CORE_NEW_ACTION__REUSED_ACTIONS);

        coreModelElementMappingEClass = createEClass(CORE_MODEL_ELEMENT_MAPPING);
        createEReference(coreModelElementMappingEClass, CORE_MODEL_ELEMENT_MAPPING__FROM);
        createEReference(coreModelElementMappingEClass, CORE_MODEL_ELEMENT_MAPPING__TO);
        createEReference(coreModelElementMappingEClass, CORE_MODEL_ELEMENT_MAPPING__TYPE);

        toCardinalityEClass = createEClass(TO_CARDINALITY);
        createEAttribute(toCardinalityEClass, TO_CARDINALITY__TO_CARDINALITY);
        createEReference(toCardinalityEClass, TO_CARDINALITY__ELEMENT_MAPPING);
        createEReference(toCardinalityEClass, TO_CARDINALITY__LANGUAGE_ELEMENT);

        languageMapEClass = createEClass(LANGUAGE_MAP);
        createEAttribute(languageMapEClass, LANGUAGE_MAP__KEY);
        createEReference(languageMapEClass, LANGUAGE_MAP__VALUE);

        artefactMapEClass = createEClass(ARTEFACT_MAP);
        createEReference(artefactMapEClass, ARTEFACT_MAP__VALUE);
        createEAttribute(artefactMapEClass, ARTEFACT_MAP__KEY);

        // Create enums
        coreFeatureRelationshipTypeEEnum = createEEnum(CORE_FEATURE_RELATIONSHIP_TYPE);
        coreVisibilityTypeEEnum = createEEnum(CORE_VISIBILITY_TYPE);
        corePartialityTypeEEnum = createEEnum(CORE_PARTIALITY_TYPE);
        coreRelationshipEEnum = createEEnum(CORE_RELATIONSHIP);
        cardinalityEEnum = createEEnum(CARDINALITY);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Create type parameters
        ETypeParameter coreLinkEClass_T = addETypeParameter(coreLinkEClass, "T");
        addETypeParameter(coreModelElementCompositionEClass, "T");
        ETypeParameter coreMappingEClass_T = addETypeParameter(coreMappingEClass, "T");

        // Set bounds for type parameters

        // Add supertypes to classes
        coreArtefactEClass.getESuperTypes().add(this.getCORENamedElement());
        coreImpactModelEClass.getESuperTypes().add(this.getCOREArtefact());
        coreConcernEClass.getESuperTypes().add(this.getCORENamedElement());
        coreFeatureEClass.getESuperTypes().add(this.getCORENamedElement());
        EGenericType g1 = createEGenericType(this.getCOREModelElementComposition());
        EGenericType g2 = createEGenericType(coreLinkEClass_T);
        g1.getETypeArguments().add(g2);
        coreLinkEClass.getEGenericSuperTypes().add(g1);
        coreReuseEClass.getESuperTypes().add(this.getCORENamedElement());
        coreImpactNodeEClass.getESuperTypes().add(this.getCORENamedElement());
        coreConfigurationEClass.getESuperTypes().add(this.getCOREModelComposition());
        coreConfigurationEClass.getESuperTypes().add(this.getCORENamedElement());
        coreFeatureModelEClass.getESuperTypes().add(this.getCOREArtefact());
        coreModelReuseEClass.getESuperTypes().add(this.getCOREModelComposition());
        coreFeatureImpactNodeEClass.getESuperTypes().add(this.getCOREImpactNode());
        g1 = createEGenericType(this.getCORELink());
        g2 = createEGenericType(this.getCOREImpactNode());
        g1.getETypeArguments().add(g2);
        coreWeightedLinkEClass.getEGenericSuperTypes().add(g1);
        coreModelExtensionEClass.getESuperTypes().add(this.getCOREModelComposition());
        g1 = createEGenericType(this.getCORELink());
        g2 = createEGenericType(coreMappingEClass_T);
        g1.getETypeArguments().add(g2);
        coreMappingEClass.getEGenericSuperTypes().add(g1);
        coreSceneEClass.getESuperTypes().add(this.getCORENamedElement());
        corePerspectiveEClass.getESuperTypes().add(this.getCORELanguage());
        coreExternalLanguageEClass.getESuperTypes().add(this.getCORELanguage());
        corePerspectiveActionEClass.getESuperTypes().add(this.getCOREAction());
        coreExternalArtefactEClass.getESuperTypes().add(this.getCOREArtefact());
        coreReexposeActionEClass.getESuperTypes().add(this.getCOREPerspectiveAction());
        coreRedefineActionEClass.getESuperTypes().add(this.getCOREPerspectiveAction());
        coreLanguageActionEClass.getESuperTypes().add(this.getCOREAction());
        coreCreateMappingEClass.getESuperTypes().add(this.getCOREPerspectiveAction());
        binaryElementMappingEClass.getESuperTypes().add(this.getCORELanguageElementMapping());
        naryElementMappingEClass.getESuperTypes().add(this.getCORELanguageElementMapping());
        coreLanguageEClass.getESuperTypes().add(this.getCOREArtefact());
        coreNewActionEClass.getESuperTypes().add(this.getCOREPerspectiveAction());

        // Initialize classes and features; add operations and parameters
        initEClass(coreArtefactEClass, COREArtefact.class, "COREArtefact", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREArtefact_ModelReuses(), this.getCOREModelReuse(), null, "modelReuses", null, 0, -1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_CoreConcern(), this.getCOREConcern(), this.getCOREConcern_Artefacts(), "coreConcern", null, 1, 1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_ModelExtensions(), this.getCOREModelExtension(), null, "modelExtensions", null, 0, -1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_UiElements(), this.getCOREUIElement(), null, "uiElements", null, 0, -1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_CiElements(), this.getCORECIElement(), null, "ciElements", null, 0, -1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_TemporaryConcern(), this.getCOREConcern(), this.getCOREConcern_TemporaryArtefacts(), "temporaryConcern", null, 0, 1, COREArtefact.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREArtefact_Scene(), this.getCOREScene(), null, "scene", null, 0, 1, COREArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreImpactModelEClass, COREImpactModel.class, "COREImpactModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREImpactModel_ImpactModelElements(), this.getCOREImpactNode(), null, "impactModelElements", null, 0, -1, COREImpactModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREImpactModel_Layouts(), this.getLayoutContainerMap(), null, "layouts", null, 0, -1, COREImpactModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREImpactModel_Contributions(), this.getCOREContribution(), null, "contributions", null, 0, -1, COREImpactModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreConcernEClass, COREConcern.class, "COREConcern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREConcern_Artefacts(), this.getCOREArtefact(), this.getCOREArtefact_CoreConcern(), "artefacts", null, 1, -1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_FeatureModel(), this.getCOREFeatureModel(), null, "featureModel", null, 1, 1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_ImpactModel(), this.getCOREImpactModel(), null, "impactModel", null, 0, 1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_Scenes(), this.getCOREScene(), null, "scenes", null, 0, -1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_Reuses(), this.getCOREReuse(), null, "reuses", null, 0, -1, COREConcern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConcern_TemporaryArtefacts(), this.getCOREArtefact(), this.getCOREArtefact_TemporaryConcern(), "temporaryArtefacts", null, 0, -1, COREConcern.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreFeatureEClass, COREFeature.class, "COREFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREFeature_RealizedBy(), this.getCOREScene(), this.getCOREScene_Realizes(), "realizedBy", null, 0, -1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeature_Children(), this.getCOREFeature(), this.getCOREFeature_Parent(), "children", null, 0, -1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeature_Parent(), this.getCOREFeature(), this.getCOREFeature_Children(), "parent", null, 0, 1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREFeature_ParentRelationship(), this.getCOREFeatureRelationshipType(), "parentRelationship", "None", 1, 1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeature_Requires(), this.getCOREFeature(), null, "requires", null, 0, -1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeature_Excludes(), this.getCOREFeature(), null, "excludes", null, 0, -1, COREFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelCompositionEClass, COREModelComposition.class, "COREModelComposition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREModelComposition_Source(), this.getCOREArtefact(), null, "source", null, 1, 1, COREModelComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        g1 = createEGenericType(this.getCOREModelElementComposition());
        g2 = createEGenericType();
        g1.getETypeArguments().add(g2);
        initEReference(getCOREModelComposition_Compositions(), g1, null, "compositions", null, 0, -1, COREModelComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLinkEClass, CORELink.class, "CORELink", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        g1 = createEGenericType(coreLinkEClass_T);
        initEReference(getCORELink_To(), g1, null, "to", null, 1, 1, CORELink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        g1 = createEGenericType(coreLinkEClass_T);
        initEReference(getCORELink_From(), g1, null, "from", null, 1, 1, CORELink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreNamedElementEClass, CORENamedElement.class, "CORENamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCORENamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, CORENamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreReuseEClass, COREReuse.class, "COREReuse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREReuse_ReusedConcern(), this.getCOREConcern(), null, "reusedConcern", null, 1, 1, COREReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREReuse_Extends(), this.getCOREReuse(), null, "extends", null, 0, 1, COREReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREReuse_ModelReuses(), this.getCOREModelReuse(), this.getCOREModelReuse_Reuse(), "modelReuses", null, 0, -1, COREReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreImpactNodeEClass, COREImpactNode.class, "COREImpactNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREImpactNode_ScalingFactor(), ecorePackage.getEFloat(), "scalingFactor", null, 1, 1, COREImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREImpactNode_Offset(), ecorePackage.getEFloat(), "offset", null, 1, 1, COREImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREImpactNode_Outgoing(), this.getCOREContribution(), this.getCOREContribution_Source(), "outgoing", null, 0, -1, COREImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREImpactNode_Incoming(), this.getCOREContribution(), this.getCOREContribution_Impacts(), "incoming", null, 0, -1, COREImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreConfigurationEClass, COREConfiguration.class, "COREConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREConfiguration_Selected(), this.getCOREFeature(), null, "selected", null, 0, -1, COREConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConfiguration_Reexposed(), this.getCOREFeature(), null, "reexposed", null, 0, -1, COREConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConfiguration_ExtendingConfigurations(), this.getCOREConfiguration(), null, "extendingConfigurations", null, 0, -1, COREConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREConfiguration_ExtendedReuse(), this.getCOREReuse(), null, "extendedReuse", null, 0, 1, COREConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreFeatureModelEClass, COREFeatureModel.class, "COREFeatureModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREFeatureModel_Features(), this.getCOREFeature(), null, "features", null, 0, -1, COREFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeatureModel_Root(), this.getCOREFeature(), null, "root", null, 1, 1, COREFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelReuseEClass, COREModelReuse.class, "COREModelReuse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREModelReuse_Reuse(), this.getCOREReuse(), this.getCOREReuse_ModelReuses(), "reuse", null, 1, 1, COREModelReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREModelReuse_Configuration(), this.getCOREConfiguration(), null, "configuration", null, 1, 1, COREModelReuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreContributionEClass, COREContribution.class, "COREContribution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREContribution_RelativeWeight(), ecorePackage.getEInt(), "relativeWeight", null, 1, 1, COREContribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREContribution_Source(), this.getCOREImpactNode(), this.getCOREImpactNode_Outgoing(), "source", null, 1, 1, COREContribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREContribution_Impacts(), this.getCOREImpactNode(), this.getCOREImpactNode_Incoming(), "impacts", null, 1, 1, COREContribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(layoutMapEClass, Map.Entry.class, "LayoutMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEReference(getLayoutMap_Key(), ecorePackage.getEObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLayoutMap_Value(), this.getLayoutElement(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(layoutElementEClass, LayoutElement.class, "LayoutElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLayoutElement_X(), ecorePackage.getEFloat(), "x", "0.0", 1, 1, LayoutElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLayoutElement_Y(), ecorePackage.getEFloat(), "y", "0.0", 1, 1, LayoutElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(layoutContainerMapEClass, Map.Entry.class, "LayoutContainerMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEReference(getLayoutContainerMap_Key(), ecorePackage.getEObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLayoutContainerMap_Value(), this.getLayoutMap(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreFeatureImpactNodeEClass, COREFeatureImpactNode.class, "COREFeatureImpactNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREFeatureImpactNode_RelativeFeatureWeight(), ecorePackage.getEInt(), "relativeFeatureWeight", null, 1, 1, COREFeatureImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeatureImpactNode_Represents(), this.getCOREFeature(), null, "represents", null, 1, 1, COREFeatureImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREFeatureImpactNode_WeightedLinks(), this.getCOREWeightedLink(), null, "weightedLinks", null, 0, -1, COREFeatureImpactNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelElementCompositionEClass, COREModelElementComposition.class, "COREModelElementComposition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(coreWeightedLinkEClass, COREWeightedLink.class, "COREWeightedLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREWeightedLink_Weight(), ecorePackage.getEInt(), "weight", null, 1, 1, COREWeightedLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelExtensionEClass, COREModelExtension.class, "COREModelExtension", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(coreMappingEClass, COREMapping.class, "COREMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        g1 = createEGenericType(this.getCOREMapping());
        g2 = createEGenericType();
        g1.getETypeArguments().add(g2);
        initEReference(getCOREMapping_Mappings(), g1, null, "mappings", null, 0, -1, COREMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        g1 = createEGenericType(this.getCOREMapping());
        g2 = createEGenericType();
        g1.getETypeArguments().add(g2);
        initEReference(getCOREMapping_ReferencedMappings(), g1, null, "referencedMappings", null, 0, -1, COREMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreSceneEClass, COREScene.class, "COREScene", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREScene_Realizes(), this.getCOREFeature(), this.getCOREFeature_RealizedBy(), "realizes", null, 0, -1, COREScene.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREScene_PerspectiveName(), ecorePackage.getEString(), "perspectiveName", null, 0, 1, COREScene.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREScene_ElementMappings(), this.getCOREModelElementMapping(), null, "elementMappings", null, 0, -1, COREScene.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREScene_Artefacts(), this.getArtefactMap(), null, "artefacts", null, 0, -1, COREScene.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(corePerspectiveEClass, COREPerspective.class, "COREPerspective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREPerspective_Mappings(), this.getCORELanguageElementMapping(), null, "mappings", null, 0, -1, COREPerspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREPerspective_Languages(), this.getLanguageMap(), null, "languages", null, 0, -1, COREPerspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREPerspective_Default(), ecorePackage.getEString(), "default", null, 0, 1, COREPerspective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreExternalLanguageEClass, COREExternalLanguage.class, "COREExternalLanguage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREExternalLanguage_NsURI(), ecorePackage.getEString(), "nsURI", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_ResourceFactory(), ecorePackage.getEString(), "resourceFactory", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_AdapterFactory(), ecorePackage.getEString(), "adapterFactory", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_WeaverClassName(), ecorePackage.getEString(), "weaverClassName", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalLanguage_FileExtension(), ecorePackage.getEString(), "fileExtension", null, 1, 1, COREExternalLanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(corePerspectiveActionEClass, COREPerspectiveAction.class, "COREPerspectiveAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(coreExternalArtefactEClass, COREExternalArtefact.class, "COREExternalArtefact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREExternalArtefact_RootModelElement(), ecorePackage.getEObject(), null, "rootModelElement", null, 1, 1, COREExternalArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREExternalArtefact_LanguageName(), ecorePackage.getEString(), "languageName", null, 0, 1, COREExternalArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreuiElementEClass, COREUIElement.class, "COREUIElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREUIElement_ModelElement(), ecorePackage.getEObject(), null, "modelElement", null, 1, 1, COREUIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreciElementEClass, CORECIElement.class, "CORECIElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCORECIElement_Partiality(), this.getCOREPartialityType(), "partiality", null, 0, 1, CORECIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECIElement_ModelElement(), ecorePackage.getEObject(), null, "modelElement", null, 1, 1, CORECIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECIElement_MappingCardinality(), this.getCOREMappingCardinality(), null, "mappingCardinality", null, 0, 1, CORECIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECIElement_ReferenceCardinality(), this.getCOREMappingCardinality(), null, "referenceCardinality", null, 0, -1, CORECIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreMappingCardinalityEClass, COREMappingCardinality.class, "COREMappingCardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREMappingCardinality_LowerBound(), ecorePackage.getEInt(), "lowerBound", "0", 1, 1, COREMappingCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREMappingCardinality_UpperBound(), ecorePackage.getEInt(), "upperBound", "1", 1, 1, COREMappingCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCOREMappingCardinality_Name(), ecorePackage.getEString(), "name", null, 0, 1, COREMappingCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLanguageElementMappingEClass, CORELanguageElementMapping.class, "CORELanguageElementMapping", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCORELanguageElementMapping_Relationship(), this.getCORERelationship(), "relationship", null, 1, 1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElementMapping_Actions(), this.getCORECreateMapping(), this.getCORECreateMapping_Type(), "actions", null, 0, -1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCORELanguageElementMapping_FromCardinality(), this.getCardinality(), "fromCardinality", null, 1, 1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElementMapping_Instances(), this.getCOREModelElementMapping(), this.getCOREModelElementMapping_Type(), "instances", null, 0, -1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORELanguageElementMapping_From(), ecorePackage.getEObject(), null, "from", null, 1, 1, CORELanguageElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreReexposeActionEClass, COREReexposeAction.class, "COREReexposeAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREReexposeAction_ReexposedAction(), this.getCOREAction(), null, "reexposedAction", null, 1, 1, COREReexposeAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreRedefineActionEClass, CORERedefineAction.class, "CORERedefineAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORERedefineAction_RedefinedAction(), this.getCOREAction(), null, "redefinedAction", null, 1, 1, CORERedefineAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORERedefineAction_ReusedActions(), this.getCOREAction(), null, "reusedActions", null, 1, -1, CORERedefineAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLanguageActionEClass, CORELanguageAction.class, "CORELanguageAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(coreCreateMappingEClass, CORECreateMapping.class, "CORECreateMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORECreateMapping_Type(), this.getCORELanguageElementMapping(), this.getCORELanguageElementMapping_Actions(), "type", null, 1, 1, CORECreateMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCORECreateMapping_ExtendedAction(), this.getCORECreateMapping(), null, "extendedAction", null, 0, 1, CORECreateMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(binaryElementMappingEClass, BinaryElementMapping.class, "BinaryElementMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getBinaryElementMapping_ToCardinality(), this.getCardinality(), "toCardinality", null, 1, 1, BinaryElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getBinaryElementMapping_To(), ecorePackage.getEObject(), null, "to", null, 1, 1, BinaryElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(naryElementMappingEClass, NaryElementMapping.class, "NaryElementMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getNaryElementMapping_Cardinalities(), this.getToCardinality(), this.getToCardinality_ElementMapping(), "cardinalities", null, 0, -1, NaryElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreLanguageEClass, CORELanguage.class, "CORELanguage", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORELanguage_Actions(), this.getCOREAction(), null, "actions", null, 0, -1, CORELanguage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreActionEClass, COREAction.class, "COREAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCOREAction_Name(), ecorePackage.getEString(), "name", null, 0, 1, COREAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreNewActionEClass, CORENewAction.class, "CORENewAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCORENewAction_ReusedActions(), this.getCOREAction(), null, "reusedActions", null, 0, -1, CORENewAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(coreModelElementMappingEClass, COREModelElementMapping.class, "COREModelElementMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCOREModelElementMapping_From(), ecorePackage.getEObject(), null, "from", null, 1, 1, COREModelElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREModelElementMapping_To(), ecorePackage.getEObject(), null, "to", null, 1, 1, COREModelElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCOREModelElementMapping_Type(), this.getCORELanguageElementMapping(), this.getCORELanguageElementMapping_Instances(), "type", null, 1, 1, COREModelElementMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(toCardinalityEClass, ToCardinality.class, "ToCardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getToCardinality_ToCardinality(), this.getCardinality(), "toCardinality", null, 1, 1, ToCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getToCardinality_ElementMapping(), this.getNaryElementMapping(), this.getNaryElementMapping_Cardinalities(), "elementMapping", null, 1, 1, ToCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getToCardinality_LanguageElement(), ecorePackage.getEObject(), null, "languageElement", null, 1, 1, ToCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(languageMapEClass, Map.Entry.class, "LanguageMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLanguageMap_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLanguageMap_Value(), this.getCORELanguage(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(artefactMapEClass, Map.Entry.class, "ArtefactMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEReference(getArtefactMap_Value(), this.getCOREArtefact(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getArtefactMap_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.class, "COREFeatureRelationshipType");
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.NONE);
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.OPTIONAL);
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.MANDATORY);
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.XOR);
        addEEnumLiteral(coreFeatureRelationshipTypeEEnum, COREFeatureRelationshipType.OR);

        initEEnum(coreVisibilityTypeEEnum, COREVisibilityType.class, "COREVisibilityType");
        addEEnumLiteral(coreVisibilityTypeEEnum, COREVisibilityType.PUBLIC);
        addEEnumLiteral(coreVisibilityTypeEEnum, COREVisibilityType.CONCERN);

        initEEnum(corePartialityTypeEEnum, COREPartialityType.class, "COREPartialityType");
        addEEnumLiteral(corePartialityTypeEEnum, COREPartialityType.NONE);
        addEEnumLiteral(corePartialityTypeEEnum, COREPartialityType.PUBLIC);
        addEEnumLiteral(corePartialityTypeEEnum, COREPartialityType.CONCERN);

        initEEnum(coreRelationshipEEnum, CORERelationship.class, "CORERelationship");
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.EQUALITY);
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.REFINES);
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.USES);
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.EXTENDS);
        addEEnumLiteral(coreRelationshipEEnum, CORERelationship.REQUIRES);

        initEEnum(cardinalityEEnum, Cardinality.class, "Cardinality");
        addEEnumLiteral(cardinalityEEnum, Cardinality.COMPULSORY);
        addEEnumLiteral(cardinalityEEnum, Cardinality.OPTIONAL);
        addEEnumLiteral(cardinalityEEnum, Cardinality.COMPULSORY_MULTIPLE);
        addEEnumLiteral(cardinalityEEnum, Cardinality.OPTIONAL_MULTIPLE);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http://www.eclipse.org/OCL/Import
        createImportAnnotations();
        // http://www.eclipse.org/emf/2002/Ecore
        createEcoreAnnotations();
        // http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
        createPivotAnnotations();
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/OCL/Import</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void createImportAnnotations() {
        String source = "http://www.eclipse.org/OCL/Import";
        addAnnotation
          (this,
           source,
           new String[] {
               "ecore", "http://www.eclipse.org/emf/2002/Ecore"
           });
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void createEcoreAnnotations() {
        String source = "http://www.eclipse.org/emf/2002/Ecore";
        addAnnotation
          (this,
           source,
           new String[] {
               "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
               "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
               "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
           });
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void createPivotAnnotations() {
        String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";
        addAnnotation
          (getCOREConcern_FeatureModel(),
           source,
           new String[] {
               "derivation", "if self.artefacts->exists(a | a.oclIsTypeOf(COREFeatureModel)) then\n                self.artefacts->any(a | a.oclIsTypeOf(COREFeatureModel)).oclAsType(COREFeatureModel)\n            else\n                null\n            endif"
           });
        addAnnotation
          (getCOREConcern_ImpactModel(),
           source,
           new String[] {
               "derivation", "\n            if self.artefacts->exists(a | a.oclIsTypeOf(COREImpactModel)) then\n                self.artefacts->any(a | a.oclIsTypeOf(COREImpactModel)).oclAsType(COREImpactModel)\n            else\n                null\n            endif"
           });
    }

} //CorePackageImpl
