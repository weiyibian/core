/**
 */
package ca.mcgill.sel.core;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Language Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORELanguageAction()
 * @model
 * @generated
 */
public interface CORELanguageAction extends COREAction {

} // CORELanguageAction
