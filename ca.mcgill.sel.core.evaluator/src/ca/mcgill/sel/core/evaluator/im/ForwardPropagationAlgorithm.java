package ca.mcgill.sel.core.evaluator.im;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREImpactModelUtil;

/**
 * This class conducts the forward propagation of satisfaction values.
 * @author berkduran
 *
 */
public final class ForwardPropagationAlgorithm {

    /**
     * PROPAGATION_SCALE is also used by the scale factor and offset calculation.
     */
    static final float PROPAGATION_SCALE = 100.0f;
    private static final float SELECTED_EVALUATION_VALUE = 100.0f;
    private static final float UNSELECTED_EVALUATION_VALUE = 0.0f;
    private static final float MAX_POSSIBLE_EVALUATION_VALUE = 100.0f;
    private static final float MIN_POSSIBLE_EVALUATION_VALUE = 0.0f;
    private static final float EVALUATION_TOLERANCE_VALUE = 0.5f;

    /**
     * Constructor.
     */
    private ForwardPropagationAlgorithm() {
    }

    /**
     * This is the function to conduct the forward propagation.
     * @param impactModel is the impact model at the top level that is being displayed.
     * @param configuration is COREReuseConfiguration that contains info from lower levels (reused models) too.
     * @param rootGoals are the goals satisfaction levels of which are desired to be evaluated.
     * @return a PropagationResult that contains goal/featureimpact -> satisfaction value with reuse hierarchy.
     */
    public static PropagationResult propagate(COREImpactModel impactModel,
            COREConfiguration configuration,
            List<COREImpactNode> rootGoals) {

        PropagationResult evaluations = new PropagationResult();
        List<COREImpactNode> elementsReady = new ArrayList<>();
        List<COREImpactNode> elementsWaiting = new ArrayList<>();
        Map<COREReuse, List<COREImpactNode>> elementsReused = new HashMap<>();
        Map<COREImpactNode, Integer> evaluationCounter = new HashMap<>();

        for (COREImpactNode aRootGoal : rootGoals) {
            evaluationCounter.put(aRootGoal, aRootGoal.getIncoming().size());
            for (COREImpactNode aDescendant : COREImpactModelUtil.getDescendants(aRootGoal)) {
                if (!evaluationCounter.containsKey(aDescendant)) {
                    evaluationCounter.put(aDescendant, aDescendant.getIncoming().size());
                }
            }
        }
        // Now go through the map and fill the three lists: reused - ready - waiting
        for (COREImpactNode element : evaluationCounter.keySet()) {
            // String strAdded = " added to ";
            if (element instanceof COREFeatureImpactNode) {
                COREFeatureImpactNode elementNode = (COREFeatureImpactNode) element;
                //Should check if it has a weighted mapping and if the feature reusing it is selected.
                if ((!elementNode.getWeightedLinks().isEmpty()) 
                        && (configuration.getSelected().contains(elementNode.getRepresents()))) {
                    for (COREWeightedLink aLink : elementNode.getWeightedLinks()) {
                        COREModelReuse modelReuse =
                                EMFModelUtil.getRootContainerOfType(aLink, CorePackage.Literals.CORE_MODEL_REUSE);
                        if (elementsReused.containsKey(modelReuse.getReuse())) {
                            elementsReused.get(modelReuse.getReuse()).add(aLink.getFrom());
                        } else {
                            List<COREImpactNode> reusedGoals = new ArrayList<>();
                            reusedGoals.add(aLink.getFrom());
                            elementsReused.put(modelReuse.getReuse(), reusedGoals);
                        }
                    }
                }
                //strAdded += "Reused.";
            }

            if (evaluationCounter.get(element) == 0) {
                elementsReady.add(element);
                // strAdded += "Ready.";
            } else {
                elementsWaiting.add(element);
                // strAdded += "Waiting.";
            }
            // System.out.println("+++ " + element.getName() + " -Incoming: " + evaluationCounter.get(element) +
            // strAdded);
        }

        // 1- Start with the list of reused elements, pass the evaluation map down to fill.
        // The function will call itself again for the reused element.
        // only if the reusing feature is in the feature selection.
        for (COREReuse aReuse : elementsReused.keySet()) {
            List<COREImpactNode> reusedRootGoals = new ArrayList<>();
            reusedRootGoals.addAll(elementsReused.get(aReuse));
            COREImpactModel reusedImpactModel = EMFModelUtil.getRootContainerOfType(reusedRootGoals.get(0), 
                    CorePackage.Literals.CORE_IMPACT_MODEL);
            COREConfiguration reusedConfiguration = null;
            for (COREConfiguration aConfig : configuration.getExtendingConfigurations()) {
                if (((COREModelReuse) (aConfig.eContainer())).getReuse().equals(aReuse)) {
                    reusedConfiguration = aConfig;
                } 
            }
                        
            PropagationResult resultsForAReuse = propagate(reusedImpactModel, reusedConfiguration, reusedRootGoals);
            evaluations.getReuseEvaluations().put(aReuse, resultsForAReuse);
        }
        
        // 2- Once all the reused elements are evaluated, switch to the ready elements and conduct normal evaluation.
        while (elementsReady.size() > 0) {
            COREImpactNode element = elementsReady.get(0);
            elementsReady.remove(element);
            //evaluations.put(element, calculateEvaluation(element, configuration, evaluations));
            evaluations.getEvaluations().put(element, calculateEvaluation(element, configuration, evaluations));

            // 3- After evaluating a ready element, update the evaluation counter and wait list.
            for (COREContribution outgoingLink : element.getOutgoing()) {
                COREImpactNode contributed = outgoingLink.getImpacts();
                if (evaluationCounter.containsKey(contributed)) {
                    int newCount = evaluationCounter.get(contributed) - 1;
                    evaluationCounter.put(contributed, newCount);
                    if (newCount == 0) {
                        elementsWaiting.remove(contributed);
                        elementsReady.add(contributed);
                    }
                }
            }
        }
        return evaluations;
    }
    
    /**
     * This function returns the satisfaction value of a given goal model element.
     * @param intElement is a goal, feature or a featureimpactnode.
     * @param configuration is the feature configuration for the intElement's feature model.
     * @param evaluations is the result class whose maps are being filled hierarchically throughout the evaluation.
     * @return the satisfaction value for the intElement.
     */
    private static float calculateEvaluation(COREImpactNode intElement, COREConfiguration configuration,
            PropagationResult evaluations) {
        float result = UNSELECTED_EVALUATION_VALUE;

        if (intElement instanceof COREFeatureImpactNode) {
            COREFeatureImpactNode featureImpact = (COREFeatureImpactNode) intElement;
            if (configuration.getSelected().contains(featureImpact.getRepresents())) {
                if (featureImpact.getWeightedLinks().isEmpty()) {
                    result = SELECTED_EVALUATION_VALUE;
                } else {
                    //if this is a special node, find the values from the table and apply.
                    //since this is a selected feature, first add its own weight.
                    result += featureImpact.getRelativeFeatureWeight() * SELECTED_EVALUATION_VALUE;
                    //satisfaction values of the reused features should have been already added to the table.
                    for (COREWeightedLink aLink : featureImpact.getWeightedLinks()) {
                        //Get the reuse from the mapping and look for its evaluations using that reuse
                        COREModelReuse modelReuse =
                                EMFModelUtil.getRootContainerOfType(aLink, CorePackage.Literals.CORE_MODEL_REUSE);
                        PropagationResult evalForReuse = evaluations.getEvaluationsForReuse(modelReuse.getReuse());
                        Float evalForNode = evalForReuse.getEvaluationForImpactNode(aLink.getFrom());
                        result += evalForNode * aLink.getWeight();
                    }
                    result = result / PROPAGATION_SCALE;
                    // Apply scaling factor and offset for this element.
                    result = ImpactModelEvaluationUtil.scaleAndOffsetSatisfaction(result, 
                            featureImpact.getScalingFactor(), featureImpact.getOffset());
                }
            }
        } else {
            for (COREContribution oneLink : intElement.getIncoming()) {
                result += evaluations.getEvaluationForImpactNode(oneLink.getSource()) * oneLink.getRelativeWeight();
            }
            result = result / PROPAGATION_SCALE;
            // Apply scaling factor and offset for this element.
            result = ImpactModelEvaluationUtil.scaleAndOffsetSatisfaction(result, 
                    intElement.getScalingFactor(), intElement.getOffset());
        }

        // Finally, ensure the result is within the allowed range and round up/down if necessary.
        if (result >= MAX_POSSIBLE_EVALUATION_VALUE - EVALUATION_TOLERANCE_VALUE) {
            result = MAX_POSSIBLE_EVALUATION_VALUE;
        } else if (result <= MIN_POSSIBLE_EVALUATION_VALUE + EVALUATION_TOLERANCE_VALUE) {
            result = MIN_POSSIBLE_EVALUATION_VALUE;
        }

        return result;
    }
}
