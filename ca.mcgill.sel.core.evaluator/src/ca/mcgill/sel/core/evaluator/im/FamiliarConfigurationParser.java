package ca.mcgill.sel.core.evaluator.im;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to parse familiar configuration and split into features
 * features are separated by spaces and double quotes.
 * @author berkduran
 *
 */
public final class FamiliarConfigurationParser {

    /**
     * The configPattern will match on either quoted text or text between space, including
     * whitespace, and accounting for beginning and end of line.
     */
    private static final Pattern CONFIG_PATTERN = Pattern.compile("\"([^\"]*)\"|(?<= |^)([^ ]*)(?: |$)");  

    /**
     * Constructor.
     */
    private FamiliarConfigurationParser() {
    }

    /**
     * Parse configuration to extract features divided by whitespaces and double quotes.
     * @param configurationString 
     * @return array of feature names
     */
    public static String[] parseConfig(String configurationString) {
        Matcher matcher = CONFIG_PATTERN.matcher(configurationString);
        ArrayList<String> allMatches = new ArrayList<>(); 
        allMatches.clear();
        String match;
        while (matcher.find()) {
            match = matcher.group(1);
            if (match != null) {
                allMatches.add(match);
            } else {
                allMatches.add(matcher.group(2));
            }
        }
        List<String> results = new ArrayList<>();
        for (int i = 0; i < allMatches.size(); i++) {
            if (!"".equals(allMatches.get(i))) {
                results.add(allMatches.get(i).trim());
            }
        }
        if (results.size() > 0) {
            return results.toArray(new String[results.size()]);
        } else {
            return new String[0];
        }                       
    }   
}